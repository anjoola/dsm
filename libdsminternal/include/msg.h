#ifndef _MSG_H_
#define _MSG_H_

/**
 * Common code for message passing between application, server, and daemon.
 *
 * Include definitions of message layouts, and prototypes of functions to
 * send and receive these messages over an established TCP connection with
 * a file descriptor.
 */
#include <stdint.h>
#include <arpa/inet.h>

/**
 * Message types
 * - First 3 letters specify type:
 *     MSG - First message sent of a pair.
 *     RSP - Response, with body, to a MSG
 *     ACK - Response, without body, to a MSG (has empty body[])
 * - Next two 3-letter parts are sender, then receiver
 *     APP - Application
 *     SRV - Server
 *     DMN - Daemon
 * - Everything after is message type name.
 */
typedef enum {
  MSG_SRV_DMN_DEPLOY,
  MSG_SRV_DMN_KILL,

  MSG_APP_SRV_INIT,
  RSP_SRV_APP_DETAILS,

  MSG_APP_SRV_GETLOCK,
  RSP_SRV_APP_GETLOCK,
  MSG_APP_SRV_UNLOCK,
  RSP_SRV_APP_UNLOCK,

  MSG_APP_SRV_READPAGE,
  RSP_SRV_APP_READPAGE,

  MSG_APP_SRV_UPDATEPAGE,
  RSP_SRV_APP_UPDATEPAGE,

  MSG_APP_APP_GETPAGE,
  RSP_APP_APP_GETPAGE,

  MSG_APP_APP_WAIT, // App sends to page server signifying it is ready to quit.
  RSP_APP_APP_WAIT, // Page server sends back to app when all workers ready
                    // to quit.

  MSG_APP_SRV_WAIT, // Worker sends to master telling it to quit.
  RSP_SRV_APP_WAIT,

  NUM_MSG_TYPES     // For diagnostic use, as an interface version check.
} msg_type_t;

/**
 * Message wrapper. This is the struct that is sent via TCP stream. The
 * body[] contains the specialized payload portion.
 */
typedef struct {
  uint32_t type;   // Message type, of type msg_type_t
  uint32_t size;   // Total size of message (including header)
  uint32_t hash;   // UNUSED - hash of the whole packet (with this set to 0)
  uint8_t body[];  // Body, whose type is one of the structs below
} __attribute__((packed)) msg_container_t;

#define MSG_BODY_SIZE(msg) ((msg).size - sizeof(msg_container_t))
#define MSG_TYPE_SIZE(body_type) (sizeof(msg_container_t) + sizeof(body_type))
#define MAKE_MSG(body_type) ((msg_container_t*) malloc(sizeof(msg_container_t) + sizeof(body_type)))


/////////////////////////////// Message Payloads ///////////////////////////////

/** DSM status. */

typedef enum {
  DSM_SUCCESS,
  DSM_FAIL
} dsm_res_status;


/** Master connection-related structs. */

typedef struct {
  uint32_t client_id;   // Client ID
  in_addr_t ip;         // Client's IP address
  uint16_t port;        // TCP port
} msg_app_srv_init;

typedef struct {
  uint16_t num_clients;
  char array[];
  /* An array of size num_clients containing the following:
       in_addr_t ip;
       uint16_t port; */
} rsp_srv_app_details;


/** Lock-related structs. */

typedef struct {
  uint32_t client_id;
  uint32_t lock;
} msg_app_srv_getlock;

typedef struct {
  dsm_res_status status;
} rsp_srv_app_getlock;

typedef struct {
  uint32_t client_id;
  uint32_t lock;
} msg_app_srv_unlock;

typedef struct {
  dsm_res_status status;
} rsp_srv_app_unlock;


/** Page-related structs. */

typedef struct {
  uint32_t client_id;
  uint32_t page_num;
  uint32_t version;
} msg_app_srv_readpage;

typedef struct {
  dsm_res_status status;
  uint32_t client_id;
  uint32_t version;
} rsp_srv_app_readpage;

typedef struct {
  uint32_t client_id;
  uint32_t page_num;
  uint32_t version;
} msg_app_srv_updatepage;

typedef struct {
  dsm_res_status status;
  uint32_t client_id;
  uint32_t version;
} rsp_srv_app_updatepage;

typedef struct {
  uint32_t client_id;
  uint32_t page_num;
} msg_app_app_getpage;

typedef struct {
  dsm_res_status status;
  uint32_t page_num;
  uint32_t client_id;
  uint32_t version;
  char page_data[];
} rsp_app_app_getpage;


/** Wait-related structs. */

typedef struct {
  uint32_t client_id;
} msg_app_app_wait;

typedef struct {
} rsp_app_app_wait;

typedef struct {
} msg_app_srv_wait;

typedef struct {
} rsp_srv_app_wait;


////////////////////////////// Message Functions ///////////////////////////////

/**
 * Structure type used to keep track of a socket, and partial message state.
 */
struct msg_socket {
  // File descriptor
  int fd;

  // Number of bytes received in unfinished message.
  uint32_t recv_count;
  // Temporary msg container buffering until we receive the size field.
  union {
    uint8_t          b[sizeof(msg_container_t)];
    msg_container_t  m;
  } recv_hdr_buf;
  // In-process buffer, after full container header received. Container
  // is copied into here after the size is known..
  uint8_t *recv_buf;
};

/**
 * Create an message socket from a socket file descriptor.
 */
struct msg_socket *msg_msock_create(int fd);

/**
 * Connects to the server server_name at port.
 *
 * server_name: The name of the server.
 * port: The server port.
 * returns: The socket fd of the server, or -1 if there is an error.
 */
struct msg_socket *msg_connect (const char *server_name, uint16_t port,
                                bool verbose);

/**
 * Connects to a given IP address at port.
 *
 * ip: IP address.
 * port: The port.
 * returns: THe socket fd, or -1 if there is an error.
 */
struct msg_socket *msg_connect_ip (in_addr_t ip, uint16_t port,
                                   bool verbose);

/** Send message. Synchronous. */
int msg_send (struct msg_socket *msock, const msg_container_t *msg);

/**
 * Receive message.
 * Currently a synchronous blocking call that keeps processing until done.
 * Returned msg memory must be freed by caller.
 */
msg_container_t *msg_recv(struct msg_socket *msock);

msg_container_t *msg_send_recv(struct msg_socket *m,
                               const msg_container_t *msg);

/** Close and free a message socket. */
void msg_msock_close_free(struct msg_socket **msock);

#endif // _MSG_H_
