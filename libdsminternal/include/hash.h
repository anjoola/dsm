#include <stdint.h>

/** Hash function for 32 bit numbers. */
uint32_t hash_int32(uint32_t a);
