#include <stdint.h>

/**
 * Simple hash function for 32 bit numbers that works.
 * According to https://gist.github.com/badboy/6267743 (originally by
 * Thomas Wang, Jan 1997 http://www.concentric.net/~Ttwang/tech/inthash.htm)
 * the following method is attributed to Knuth's "The Art of Computer
 * Programming", section 6.4.
 */
uint32_t hash_int32(uint32_t a)
{
  return a * 2654435761;  // constant = golden ratio of 2^32
}
