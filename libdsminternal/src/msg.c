#include <errno.h>
#include <ifaddrs.h>
#include <netdb.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <arpa/inet.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <sys/socket.h>

#include "msg.h"


/**
 * Debug print. Only prints if DEBUG_PRINT_YES is defined.
 */
void debug(char *fmt, ...) {
  #ifdef DEBUG_PRINT_YES
    va_list args;
    va_start(args, fmt);
    printf(fmt, args);
    va_end(args);
  #endif
}

void *Malloc(size_t size) {
  void *ret = malloc(size);
  if (!ret) {
    fprintf(stderr, "[ERROR] Malloc failed!\n");
    exit(EXIT_FAILURE);
  }
}

/**
 * Create an message socket from a socket file descriptor.
 */
struct msg_socket *msg_msock_create(int fd)
{
  if (fd < 0)
    return NULL;

  struct msg_socket *msck = Malloc(sizeof(struct msg_socket));
  msck->fd = fd;
  msck->recv_count = 0;
  msck->recv_buf = NULL;
  return msck;
}

/**
 * Connects to a given socket address.
 *
 * saddr: The socket address.
 * returns: The socket fd, or -1 if there is an error.
 */
int msg_connect_helper (struct sockaddr_in *saddr, bool verbose)
{
  // Open socket to connect.
  int sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0) {
    if (verbose)
      fprintf(stderr, "[ERROR] Could not open socket.\n");
    return -1;
  }

  // Connect to server.
  if (connect(sockfd, (struct sockaddr *) saddr, sizeof(*saddr)) < 0) {
    if (verbose)
      fprintf(stderr, "[ERROR] Could not connect to server.\n");
    return -1;
  }

  if (verbose)
    printf("Connected!\n");

  return sockfd;
}

/**
 * Connects to the server server_name at port.
 *
 * server_name: The name of the server.
 * port: The server port.
 * returns: The socket fd of the server, or -1 if there is an error.
 */
struct msg_socket *msg_connect (const char *server_name, uint16_t port,
                                bool verbose)
{
  if (verbose)
    printf("Connecting to server %s:%d...\n", server_name, port);

  // Find server.
  struct hostent *server = gethostbyname(server_name);
  if (server == NULL) {
    if (verbose)
      fprintf(stderr, "[ERROR] Could not locate server %s.\n", server_name);
    return NULL;
  }

  struct sockaddr_in saddr;
  saddr.sin_family = AF_INET;
  memcpy(&(saddr.sin_addr.s_addr), server->h_addr, server->h_length);
  saddr.sin_port = htons(port);

  int fd = msg_connect_helper(&saddr, verbose);
  if(!fd) {
    fprintf(stderr, "[ERROR] Could not connect to server %s.\n", server_name);
    return NULL;
  }
  return msg_msock_create(fd);
}

/**
 * Connects to a given IP address at port.
 *
 * ip: IP address.
 * port: The port.
 * returns: THe socket fd, or -1 if there is an error.
 */
struct msg_socket *msg_connect_ip (in_addr_t ip, uint16_t port,
                                   bool verbose)
{
  if (verbose) {
    char ip_str[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &ip, ip_str, 100);
    debug("Connecting to server %s:%d...\n", ip_str, port);
  }

  struct sockaddr_in saddr;
  saddr.sin_family = AF_INET;
  saddr.sin_addr.s_addr = ip;
  saddr.sin_port = htons(port);

  int fd = msg_connect_helper(&saddr, verbose);
  if(!fd) {
    char ip_str[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &ip, ip_str, 100);
    fprintf(stderr, "[ERROR] Could not connect to server %s.\n", ip_str);
    return NULL;
  }
  return msg_msock_create(fd);
}

/** Close and free a message socket. */
void msg_msock_close_free(struct msg_socket **msock)
{
  if ((*msock)->fd < 0)
    return;
  shutdown((*msock)->fd, SHUT_WR);
  close((*msock)->fd);
  if((*msock)->recv_buf)
    free((*msock)->recv_buf);
  free(*msock);
  *msock = NULL;
}

/** Send message. Synchronous. */
int msg_send (struct msg_socket *msock, const msg_container_t *msg)
{
  debug("Sending message, size %u\n", msg->size);

  // Note that if we use non-blocking I/O, this will not always send
  // the full amount of data
  if(write(msock->fd, msg, msg->size) < 0) {
    fprintf(stderr, "socket write error: %s\n", strerror(errno));
    return -1;
  }

  return 0;
}


//////////////////////////////// RECEIVER ///////////////////////////////////


// Abstraction for total message size, since I keep flip flopping between
// including the container fields in the count or not.
#define MSG_SIZE(msg_cont) ((msg_cont).size)

/**
 * Receive message.
 * Currently a synchronous blocking call that keeps processing until done.
 * Returned msg memory must be freed by caller.
 * Returns NULL if connection closed.
 */
msg_container_t *msg_recv(struct msg_socket *msock)
{
  ssize_t bytes;

  while (true) {
    // TODO do polling

    // If working on getting container header, don't know size yet, so
    // try and finish getting the header in fixed size buffer.
    if (msock->recv_count < sizeof(msg_container_t)) {
      bytes = read(msock->fd, &msock->recv_hdr_buf.b[msock->recv_count],
                   sizeof(msg_container_t) - msock->recv_count);
      if (bytes < 0) {
        fprintf(stderr, "recv_msg read header error: %s\n", strerror(errno));
        // TODO or should just return NULL instead?
        exit(EXIT_FAILURE);
      }
      else if (bytes == 0) {
debug("[ERROR] recv_msg got 0 bytes of header, socket closed\n"); // TODO remove
        return NULL; // Stream closed.
      }
      msock->recv_count += bytes;
debug("%%%%%%recv_msg got %zu bytes of header\n", bytes);
      // If now have whole container header, allocate buffer for entire
      // message, and copy the container header over to it.
      if (msock->recv_count == sizeof(msg_container_t)) {
        msock->recv_buf = Malloc(MSG_SIZE(msock->recv_hdr_buf.m));
        memcpy(msock->recv_buf, msock->recv_hdr_buf.b,
               sizeof(msg_container_t));
      }
    }

    // Break loop if done with message
    msg_container_t *m = (void *)msock->recv_buf;
    if (msock->recv_count >= sizeof(msg_container_t)
        && msock->recv_count == m->size)
      break;

    // If done with header, work on getting rest of message
    if (msock->recv_count >= sizeof(msg_container_t)) {
      bytes = read(msock->fd, &msock->recv_buf[msock->recv_count],
                   MSG_SIZE(*m) - msock->recv_count);
      if (bytes  < 0) {
        fprintf(stderr, "recv_msg read payload error: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
      }
      else if (bytes == 0) {
debug("[ERROR] recv_msg got 0 bytes of body, socket closed\n"); // TODO remove
        free(msock->recv_buf);
        msock->recv_buf = NULL;
        return NULL;
      }
      msock->recv_count += bytes;
debug("recv_msg got %zu bytes of payload, at %u of %u\n",
       bytes, msock->recv_count, MSG_SIZE(*m));
    }
  }

debug("recv_msg done! got %u bytes\n", msock->recv_count);

  // Setup for next time
  msock->recv_count = 0;

  // Caller must free this buffer!
  msg_container_t * ret = (msg_container_t *)msock->recv_buf;
  msock->recv_buf = NULL;
  return ret;
}

msg_container_t *msg_send_recv(struct msg_socket *msock,
                               const msg_container_t *msg)
{
  if (msg_send(msock, msg) < 0){
    return NULL;
  }
  return msg_recv(msock);
}

/* Not done yet. */
#if 0
static int server_fd = -1;
static int *host_fds;

void dsm_comms_connect(const char *server_name, uint16_t server_port)
{
  printf("Connecting to server %s:%d...\n", server_host, server_port);
  server_fd = connect_to_server(server_name, server_port);
}
#endif
