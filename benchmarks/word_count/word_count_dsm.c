#include <dsm.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <sys/types.h>
#include <time.h>

void *shared_mem_addr;

#define WORD_TABLE_SIZE 1000
#define WORD_TABLE_LOCK 1
#define DONE_LOCK 2
#define THREADS 2

void print_timediff(struct timespec time_start, struct timespec time_finish)
{
  uint64_t nanodiff
       = time_finish.tv_nsec + time_finish.tv_sec * (1000*1000*1000)
       - (time_start.tv_nsec + time_start.tv_sec * (1000*1000*1000));
  int secs = nanodiff / (1000*1000*1000);
  long nsecs = nanodiff % (1000*1000*1000);

  double f_msecs = nanodiff / (1000.0 * 1000.0);

  printf("Time difference: %f msecs\n", f_msecs);
}


unsigned long hash(char *str)
{
  unsigned long hash = 5381;
  int c;
  
  while (c = *str++)
    hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
  return hash;
}

int *shared_word_table;
int *done;

void *count_words(void *id) {
  int i = 0;
  char file_name[100];
  int local_word_table[WORD_TABLE_SIZE];
  int node_id = *(int *)(id);
  printf("Host %d starting\n", node_id);

  for(i=1; i < 5; i++){
    if (i % THREADS != node_id)
      continue;

    sprintf(file_name, "file%d.txt", i);
    FILE *f = fopen(file_name, "r");
    char word[1000];
    char c;

    do {
      c = fscanf(f,"%s",word); /* got one word from the file */
      local_word_table[hash(word) % WORD_TABLE_SIZE] += 1;
    } while (c != EOF);              /* repeat until EOF           */
    fclose(f);
  }
  dsm_lock(WORD_TABLE_LOCK);
  for (i = 0; i < WORD_TABLE_SIZE; i++) {
    shared_word_table[i] += local_word_table[i];
  }
  dsm_unlock(WORD_TABLE_LOCK);

  return NULL;
}

int main(int argc, char *argv[])
{

  struct timespec time_start, time_finish;
  gettimeofday(&time_start, NULL);

  printf("Starting word_count\n");
  int i;
  int node_id = atoi(argv[3]);

  shared_mem_addr = (void*)(((size_t)9000000/getpagesize())*getpagesize());
  dsm_share(shared_mem_addr, getpagesize() * 4);
  dsm_start(argc, argv);

  srand(time(NULL) + getpid()); // Try and seed workers independently
  done = (int*)shared_mem_addr;
  shared_word_table = (int*)shared_mem_addr + 1;

  count_words(&node_id);
  dsm_lock(DONE_LOCK);
  done[0] += 1;
  dsm_unlock(DONE_LOCK);
  int done_count = 0;
  while(1) {
    dsm_lock(DONE_LOCK);
    if (done[0] == THREADS)
      break;
    dsm_unlock(DONE_LOCK);
  }
  dsm_lock(WORD_TABLE_LOCK);
  printf("##### DONE 100= %d\n", shared_word_table[100]);
  dsm_unlock(WORD_TABLE_LOCK);
  dsm_unlock(DONE_LOCK);

  dsm_wait();
  gettimeofday(&time_finish, NULL);
  print_timediff(time_start, time_finish);

  return;
}
