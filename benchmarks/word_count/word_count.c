#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <sys/types.h>
#include <time.h>
#include <pthread.h>

#define WORD_TABLE_SIZE 1000
#define WORD_TABLE_LOCK 1
#define DONE_LOCK 2
#define READ_SIZE 100000

int THREADS;

void print_timediff(struct timespec time_start, struct timespec time_finish)
{
  uint64_t nanodiff
       = time_finish.tv_nsec + time_finish.tv_sec * (1000*1000*1000)
       - (time_start.tv_nsec + time_start.tv_sec * (1000*1000*1000));
  int secs = nanodiff / (1000*1000*1000);
  long nsecs = nanodiff % (1000*1000*1000);

  double f_msecs = nanodiff / (1000.0 * 1000.0);

  printf("Time difference: %f msecs\n", f_msecs);
}

unsigned long hash(char *str)
{
  unsigned long hash = 5381;
  int c;

  while (c = *str++)
    hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
  return hash;
}

int shared_word_table[WORD_TABLE_SIZE];
pthread_mutex_t word_table_lock;

void *count_words(void *id) {
  int i = 0;
  char file_name[100];
  int local_word_table[WORD_TABLE_SIZE];
  int node_id = *(int *)(id);
  for(i=1; i < 31; i++){
    if (i % THREADS != node_id)
      continue;

    sprintf(file_name, "file%d.txt", i);
    FILE *f = fopen(file_name, "r");
    char word[1000];
    char c;

    do {
      c = fscanf(f,"%s",word); /* got one word from the file */
      local_word_table[hash(word) % WORD_TABLE_SIZE] += 1;
    } while (c != EOF);              /* repeat until EOF           */
    fclose(f);
  }
  pthread_mutex_lock(&word_table_lock);
  for (i = 0; i < WORD_TABLE_SIZE; i++) {
    shared_word_table[i] += local_word_table[i];
  }
  pthread_mutex_unlock(&word_table_lock);

  return NULL;
}


int main(int argc, char *argv[])
{
  if (argc <= 1)
    THREADS = 4;
  else
    THREADS = atoi(argv[1]);

  printf("Starting word_count\n");
  int i;
  struct timespec time_start, time_finish;
  gettimeofday(&time_start, NULL);
  pthread_t *threads = calloc(THREADS, sizeof(pthread_t));
  int *node_ids = calloc(THREADS, sizeof(pthread_t));
  pthread_mutex_init(&word_table_lock, NULL);
  for (i = 0; i < THREADS; i++) {
    node_ids[i] = i;
    pthread_create(&threads[i], NULL, count_words, &node_ids[i]);
  }

  for (i = 0; i < THREADS; i++) {
    pthread_join(threads[i], NULL);
  }

  pthread_mutex_lock(&word_table_lock);
  printf("##### DONE 100= %d\n", shared_word_table[100]);
  pthread_mutex_unlock(&word_table_lock);

  gettimeofday(&time_finish, NULL);
  print_timediff(time_start, time_finish);

  return;
}
