#include <pthread.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>

/*
 Matrix Multiply Benchmark
*/

#define DEBUG 0

#define MATRIX_FILE "matrix.txt"
#define FROM_FILE 0
#define N 1000

#define SECTION_SIZE ((N / THREADS * THREADS < N ? N / THREADS + 1 : N / THREADS))

#define get(arr, row, col) (*(arr + row * N + col))
#define set(arr, row, col, val) (*(arr + row * N + col) = val)
#define set_mod(arr, idx, val) (*(arr + idx) = val)
#define section_start(section) (SECTION_SIZE * section)

int *m1, *m2, *result;
int THREADS;

void print_timediff(struct timespec time_start, struct timespec time_finish) {
  uint64_t nanodiff
       = time_finish.tv_nsec + time_finish.tv_sec * (1000*1000*1000)
       - (time_start.tv_nsec + time_start.tv_sec * (1000*1000*1000));
  int secs = nanodiff / (1000*1000*1000);
  long nsecs = nanodiff % (1000*1000*1000);

  double f_msecs = nanodiff / (1000.0 * 1000.0);

  printf("Time difference: %f msecs\n", f_msecs);
}

void *multiply(void *section) {
  int start = section_start((int)*((int *)section));
  int end = start + SECTION_SIZE > N ? N : start + SECTION_SIZE;
  int i, j, k, sum;

  for (i = start; i < end; i++) {
    for (j = 0; j < N; j++) {
      sum = 0;
      for (k = 0; k < N; k++) {
        sum += get(m1, i, k) * get(m2, k, j);
      }
      set(result, i, j, sum);
    }
  }
  free((int *) section);
}

int main(int argc, char *argv[]) {
  struct timespec time_start, time_finish;
  gettimeofday(&time_start, NULL);

  printf("Starting Matrix Multiply test\n");

  THREADS = 4;
  if (argc > 1)
    THREADS = atoi(argv[1]);
  int i, num;

  // Create matrices.
  m1 = calloc(N * N, sizeof(int));
  m2 = calloc(N * N, sizeof(int));
  result = calloc(N * N, sizeof(int));

  // Read from file.
  if (DEBUG) printf("Reading in matrices\n");

  if (FROM_FILE) {
    FILE *file = fopen(MATRIX_FILE, "r");
    if (!file) {
      fprintf(stderr, "[ERROR] Could not open file %s\n"
                      "        Perhaps you want #define FROM_FILE 0\n",
              MATRIX_FILE);
      exit(EXIT_FAILURE);
    }
    fscanf(file, "%d", &num);
    for (i = 0; i < N * N && !feof(file); i++) {
      set_mod(m1, i, num);
      fscanf(file, "%d", &num);
    }
    for (i = 0; i < N * N && !feof(file); i++) {
      set_mod(m2, i, num);
      fscanf(file, "%d", &num);
    }
  }
  else {
    srand(time(NULL));
    for (i = 0; i < N * N; i++) {
      set_mod(m1, i, rand());
      set_mod(m2, i, rand());
    }
  }

  if (DEBUG) printf("Finished reading matrices\n");

  pthread_t *threads = calloc(THREADS, sizeof(pthread_t));
  for (i = 0; i < THREADS; i++) {
    int *thread_id = calloc(sizeof(int), 1);
    *thread_id = i;
    if (DEBUG) printf("Thread %d: Multiplying matrices\n", i);
    pthread_create(&threads[i], NULL, multiply, thread_id);
  }

  for (i = 0; i < THREADS; i++) {
    pthread_join(threads[i], NULL);
  }

  if (DEBUG) {
    int x, y;
    for (x = 0; x < N; x++) {
      for (y = 0; y < N; y++) {
        printf("%d ", get(result, x, y));
      }
      printf("\n");
    }
  }

  gettimeofday(&time_finish, NULL);
  print_timediff(time_start, time_finish);
  printf("DONE!\n");
}
