#include <dsm.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

/*
 Matrix Multiply Benchmark (DSM)
*/

#define DEBUG 0

#define MATRIX_FILE "matrix.txt"
#define FROM_FILE 0
#define N 1000

#define SECTION_SIZE ((N / NUM_HOSTS * NUM_HOSTS < N ? N / NUM_HOSTS + 1 : N / NUM_HOSTS))

#define get(arr, row, col) (*(arr + row * N + col))
#define set(arr, row, col, val) (*(arr + row * N + col) = val)
#define set_mod(arr, idx, val) (*(arr + idx) = val)
#define section_start(section) (SECTION_SIZE * section)

void *shared_mem_addr;
int *can_read_matrix;
int *num_done;
int *m1, *m2, *result;

int host_id;

int NUM_HOSTS;

#define MATRIX_READ_LOCK 999
#define MATRIX_WRITE_LOCK 1000

void print_timediff(struct timespec time_start, struct timespec time_finish) {
  uint64_t nanodiff
       = time_finish.tv_nsec + time_finish.tv_sec * (1000*1000*1000)
       - (time_start.tv_nsec + time_start.tv_sec * (1000*1000*1000));
  int secs = nanodiff / (1000*1000*1000);
  long nsecs = nanodiff % (1000*1000*1000);

  double f_msecs = nanodiff / (1000.0 * 1000.0);

  printf("Time difference: %f msecs\n", f_msecs);
}

void multiply(int section) {
  int start = section_start(section);
  int end = start + SECTION_SIZE > N ? N : start + SECTION_SIZE;
  int i, j, k, sum;

  for (i = start; i < end; i++) {
    for (j = 0; j < N; j++) {
      sum = 0;
      for (k = 0; k < N; k++) {
        sum += get(m1, i, k) * get(m2, k, j);
      }
      set(result, i, j, sum);
    }
  }

  // Synchronize. Wait until all hosts are done.
  dsm_lock(MATRIX_WRITE_LOCK);
  (*num_done)++;
  while (*num_done < NUM_HOSTS) {
    dsm_unlock(MATRIX_WRITE_LOCK);
    dsm_lock(MATRIX_WRITE_LOCK);
  }
  dsm_unlock(MATRIX_WRITE_LOCK);
}

int main(int argc, char *argv[]) {
  struct timespec time_start, time_finish;
  gettimeofday(&time_start, NULL);

  printf("Starting Matrix Multiply (DSM) test\n");

  // Shared memory setup
  long matrix_size = ((N * N) / getpagesize() + 2) * getpagesize();
  shared_mem_addr = (void*)(((size_t)9000000/getpagesize())*getpagesize());
  dsm_share(shared_mem_addr, matrix_size * 3 + getpagesize());
  dsm_start(argc, argv);

  // Setup pointers to variables in memory
  can_read_matrix = (int *) shared_mem_addr;
  *can_read_matrix = 0;
  num_done = can_read_matrix + 1;
  *num_done = 0;
  m1 = (void *) shared_mem_addr + getpagesize();
  m2 = (void *) m1 + matrix_size;
  result = (void *) m2 + matrix_size;

  if (DEBUG) printf("Shared memory set up\n");

  // Main worker reads/creates the matrix
  int num;
  long i;
  host_id = atoi(argv[argc - 2]);
  NUM_HOSTS = atoi(argv[argc - 1]);

  if (host_id == 0) {
    if (DEBUG) printf("Main Worker: Reading in matrices\n");
    dsm_lock(MATRIX_READ_LOCK);

    if (FROM_FILE) {
      FILE *file = fopen(MATRIX_FILE, "r");
      if (!file) {
        fprintf(stderr, "[ERROR] Could not open file %s\n"
                        "        Perhaps you want #define FROM_FILE 0\n",
                MATRIX_FILE);
        exit(EXIT_FAILURE);
      }
      fscanf(file, "%d", &num);
      for (i = 0; i < N * N && !feof(file); i++) {
        set_mod(m1, i, num);
        fscanf(file, "%d", &num);
      }
      for (i = 0; i < N * N && !feof(file); i++) {
        set_mod(m2, i, num);
        fscanf(file, "%d", &num);
      }
    }
    else {
      srand(time(NULL));
      for (i = 0; i < N * N; i++) {
        set_mod(m1, i, rand());
        set_mod(m2, i, rand());
      }
    }

    *can_read_matrix = 1;
    dsm_unlock(MATRIX_READ_LOCK);
    if (DEBUG) printf("Main Worker: Finished reading matrices\n");
  }
  // All other threads wait for main worker to read into memory, then lock to
  // synchronize data.
  else {
    if (DEBUG) printf("Worker %d: Waiting for master read\n", host_id);

    dsm_lock(MATRIX_READ_LOCK);
    while (!*can_read_matrix) {
      dsm_unlock(MATRIX_READ_LOCK);
      dsm_lock(MATRIX_READ_LOCK);
    }
    dsm_unlock(MATRIX_READ_LOCK);

    if (DEBUG) printf("Worker %d: Got matrices to multiply\n", host_id);
  }

  // Multiply matrices. Only multiply a small portion
  if (DEBUG) printf("Worker %d: Multiplying matrices\n", host_id);
  multiply(host_id);

  if (DEBUG) {
    int x, y;
    for (x = 0; x < N; x++) {
      for (y = 0; y < N; y++) {
        printf("%d ", get(result, x, y));
      }
      printf("\n");
    }
  }

  dsm_wait();
  gettimeofday(&time_finish, NULL);
  print_timediff(time_start, time_finish);
  printf("Worker %d: DONE!\n", host_id);

  exit(EXIT_SUCCESS);
}
