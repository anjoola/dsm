# Based on http://lists.gnu.org/archive/html/help-make/2008-04/msg00051.html
# By John Calcote

SUBDIRS = libdsminternal lib deploy

.PHONY: all clean

all clean:
	for dir in $(SUBDIRS); do \
	   echo ;\
	   echo making $$dir --------------------------------------------; \
	   echo ;\
	   $(MAKE) -C $$dir $@; \
	done
