#include <assert.h>
#include <dsm.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <sys/types.h>
#include <time.h>

#define MOD_LOOPS 10000

void *shared_mem_addr;
int *array;

int host_id;
int NUM_HOSTS;

int *num_done_hosts;

void print_timediff(struct timespec time_start, struct timespec time_finish)
{
  uint64_t nanodiff
       = time_finish.tv_nsec + time_finish.tv_sec * (1000*1000*1000)
       - (time_start.tv_nsec + time_start.tv_sec * (1000*1000*1000));
  int secs = nanodiff / (1000*1000*1000);
  long nsecs = nanodiff % (1000*1000*1000);

  double f_msecs = nanodiff / (1000.0 * 1000.0);

  printf("Time difference: %f msecs\n", f_msecs);
}

int rand_sleep(int max_milliseconds) {
  int ms = rand()%max_milliseconds;
  struct timespec t1, t2;
  t1.tv_sec = 0;
  t1.tv_nsec = ms * 1000*1000;
  nanosleep(&t1 , &t2); // If fails, don't care
  return ms;
}

// Locks
#define LA 1
#define LB 2
#define LC 3
#define LD 4

#define DONE_LOCK 5

// Data
int *A;
int *B;
int *C;
int *D;

int a_counts;
int b_counts;
int c_counts;
int d_counts;

int *a_total;
int *b_total;
int *c_total;
int *d_total;

int main(int argc, char *argv[])
{
  printf("starting test_lock_races\n");
  int i;
  for (i = 0; i < argc; i++) {
    printf("got arg: %s\n", argv[i]);
  }

  host_id = atoi(argv[argc - 2]);
  NUM_HOSTS = atoi(argv[argc - 1]);

  printf("host id: %d, number of hosts: %d\n", host_id, NUM_HOSTS);

  for (i=2; i >= 0; i--) {
    printf("Waiting %d more seconds...\n", i);
    sleep(1);
  }

  shared_mem_addr = (void*)(((size_t)9000000/getpagesize())*getpagesize());
  dsm_share(shared_mem_addr, getpagesize() * 4);
  dsm_start(argc, argv);

  srand(time(NULL) + getpid()); // Try and seed workers independently

  // Setup the data pointers, A and B on one page, C and D on another.
  A = shared_mem_addr;
  B = A + 1;
  C = A + getpagesize() / sizeof(*A);
  D = C + 1;

  a_total = A + 2 * getpagesize() / sizeof(*A);
  b_total = a_total + 1;
  c_total = a_total + 2;
  d_total = a_total + 3;

  num_done_hosts = A + 3 * getpagesize() / sizeof(*A);

  // Loop MOD_LOOPS time doing different operations.
  for(i = 0; i < MOD_LOOPS; i++) {

    // Status message
    if (i % 1000 == 0)
      printf("Completed %d of %d operations...\n", i, MOD_LOOPS);

    // Choose a random operation.
    int temp;
    switch (rand() % 11) {
    case 0:
      dsm_lock(LA);
      ++*A; ++a_counts;
      dsm_lock(LB);
      ++*B; ++b_counts;
      dsm_unlock(LB);
      dsm_unlock(LA);
      break;
    case 1:
      dsm_lock(LA);
      ++*A; ++a_counts;
      dsm_lock(LB);
      ++*B; ++b_counts;
      dsm_unlock(LB);
      dsm_unlock(LA);
      break;
    case 2:
      dsm_lock(LA);
      dsm_lock(LC);
      ++*C; ++c_counts;
      dsm_unlock(LC);
      ++*A; ++a_counts;
      dsm_unlock(LA);
      break;
    case 3:
      dsm_lock(LA);
      dsm_lock(LD);
      temp = *D;
      *D = 0;
      dsm_unlock(LD);
      *A+=temp;
      dsm_unlock(LA);
      break;
    case 4:
      dsm_lock(LB);
      temp = *B;
      *B = 0;
      dsm_lock(LD);
      *D += temp;
      dsm_unlock(LD);
      dsm_unlock(LB);
      break;
    case 5:
      dsm_lock(LA);
      temp = *A;
      *A = 0;
      dsm_unlock(LA);
      dsm_lock(LC);
      *C += temp;
      dsm_unlock(LC);
      break;
    case 6:
      dsm_lock(LC);
      temp = *C;
      *C = 0;
      dsm_unlock(LC);
      dsm_lock(LA);
      *A += temp;
      dsm_unlock(LA);
      break;
    case 7:
      dsm_lock(LB);
      --*B;
      dsm_unlock(LB);
      dsm_lock(LC);
      ++*C;
      dsm_unlock(LC);
      break;
    case 8:
      dsm_lock(LB);
      ++*B;
      dsm_unlock(LB);
      dsm_lock(LC);
      --*C;
      dsm_unlock(LC);
      break;
    case 9:
      dsm_lock(LB);
      ++*B;
      dsm_unlock(LB);
      dsm_lock(LA);
      --*A;
      dsm_unlock(LA);
      break;
    case 10:
      dsm_lock(LA);
      ++*A;
      dsm_unlock(LA);
      dsm_lock(LB);
      --*B;
      dsm_unlock(LB);
      break;
    }
    /* Note this loop does not use locks to access shared variables!
     * It's still great for debugging!. */
    //printf("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n"
    //       "Global A=%8d, B=%8d, C=%8d, D=%8d   sum=%d\n"
    //       "local    %8d,   %8d,   %8d,   %8d   sum=%d\n"
    //       "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n",
    //       *A, *B, *C, *D, *A + *B + *C + *D,
    //       a_counts, b_counts, c_counts, d_counts,
    //       a_counts + b_counts + c_counts + d_counts);
  }

  // Add the local counts, wait for everyone to finish.
  dsm_lock(LA);
  *a_total += a_counts;
  dsm_unlock(LA);

  dsm_lock(LB);
  *b_total += b_counts;
  dsm_unlock(LB);

  dsm_lock(LC);
  *c_total += c_counts;
  dsm_unlock(LC);

  dsm_lock(LD);
  *d_total += d_counts;
  dsm_unlock(LD);

  dsm_lock(DONE_LOCK);
  ++*num_done_hosts;
  while (*num_done_hosts != NUM_HOSTS) {
    dsm_unlock(DONE_LOCK);
    sleep(1);
    dsm_lock(DONE_LOCK);
  }
  dsm_unlock(DONE_LOCK);

  // Check the totals
  dsm_lock(LA);
  dsm_lock(LB);
  dsm_lock(LC);
  dsm_lock(LD);
  int total_shared = *A + *B + *C + *D;
  int total_locals = *a_total + *b_total + *c_total + *d_total;
  printf("Total counts shared: %d, all locals: %d\n", total_shared, total_locals);
  printf("Result: %s\n", total_shared == total_locals ? "PASS" : "FAIL");
  dsm_unlock(LA);
  dsm_unlock(LB);
  dsm_unlock(LC);
  dsm_unlock(LD);


  // Pause a random amount of time up to 9 seconds. This is to get each worker
  // to finish at a different time, to test dsm_wait.
  int pre_close_secs = rand() % 10;
  for (i=pre_close_secs; i >= 0; i--) {
    printf("Waiting %d more seconds...\n", i);
    sleep(1);
  }

  dsm_wait();

  printf("Done, exiting.\n");

  exit(EXIT_SUCCESS);
}
