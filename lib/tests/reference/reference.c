/*
  reference

  This is the reference application to show the very basics of using the
  DSM library.

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !!! Address Space Randomization Note !!!
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  per https://gcc.gnu.org/wiki/Randomization

  Call this on the command line first to prevent address space randomization
  in programs invoked from this bash session:

  > setarch $(uname -m) -RL bash

*/
#include <errno.h>
#include <getopt.h>
#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dsm.h>

/*
  Shared region setup

  3 pages, located at approximately halfway between stack and heap
  observed addresses on a 64 bit system. These may need to change on
  other systems. Also may not work with address randomization! See
  note at top of file.  Note the first console outputs of the program are
  pointer addresses from the stack and heap to help figure this out.
*/
void * const base = (void *)0x400000000000;
const size_t len = 3 * (4*1024);

#define COUNTER_LOCK 1
int *counter;

int main(int argc, char *argv[])
{
  // Note: The last 4 arguments of argv are for libdsm only.

  // Diagnostic message to help see where is safe to mmap.
  int s;
  int *h = malloc(sizeof(h));
  printf("stack @ %p, heap @ %p, base = %p\n", &s, h, base);


  // Setup a shared region.
  if (dsm_share(base, len)) {
    fprintf(stderr, "dsm_share error: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }

  // Begin sharing.
  if (dsm_start(argc, argv)) {
    fprintf(stderr, "dsm_start error: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }

  // Setup counter to be a place in shared memory.
  // A better program would setup linker sections for this.
  counter = base;

  // Atomically increment counter, and print the value. This printed
  // value should be unique across workers.
  dsm_lock(COUNTER_LOCK);
  ++*counter;
  printf("I just incremented counter to %d\n", *counter);
  dsm_unlock(COUNTER_LOCK);

  printf("Done!\n");

  // Wait for other workers to finish
  dsm_wait();

  printf("Exiting!\n");

  exit(0);
}
