/*
  Page Server
  -----------
  This server runs in a background thread, serving requests for memory
  chunks held by this copy of the application.
*/

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <errno.h>
#include <stdbool.h>
#include <assert.h>

#include "libdsm.h"
#include "msg.h"
#include "page_server.h"


#define MAX_EPOLL_EVENTS 10

int page_listenfd;
struct msg_socket **client_msocks;
int num_clients = 0;

/** Global variable notifies other threads that the clients are connected. */
bool dsm_page_server_all_clients_connected = false;


static void handle_getpage(msg_container_t* msg, struct msg_socket *msock);
static void handle_wait(msg_container_t* msg, struct msg_socket *msock);

// When this number equals NUM_CLIENTS, then all clients are ready to
// quit sharing memory. (But not necessarily self, since not a client.)
static int num_waiting_workers = 0;

/**
 * Open socket for other hosts to connect to.
 *
 * returns: 0 on success, -1 otherwise.
 */
int page_server_start(uint16_t port) {
  // Open socket to accept connections.
  page_listenfd = socket(AF_INET, SOCK_STREAM, 0);
  if (page_listenfd < 0) {
    fprintf(stderr, "[ERROR] Could not open socket.\n");
    return -1;
  }

  // Allow reuse of port without waiting. Otherwise get bind errors
  // on back-to-back runs.
  int one = 1;
  setsockopt(page_listenfd, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(int));

  // Bind specified port to socket.
  struct sockaddr_in saddr;
  saddr.sin_family = AF_INET;
  saddr.sin_addr.s_addr = INADDR_ANY;
  saddr.sin_port = htons(port);
  if (bind(page_listenfd, (struct sockaddr *) &saddr, sizeof(saddr)) < 0) {
    fprintf(stderr, "[ERROR] Could not bind.\n");
    return -1;
  }

  return 0;
}

/**
 * Main thread function for the page server. Serves requests from other
 * applications for memory.
 */
void *page_server(void *_args)
{
  ps_args *args = (ps_args *) _args;

  // Keep track of fds.
  num_clients = args->num_hosts - 1;
  client_msocks = calloc(num_clients, sizeof(struct msg_socket *));

  // Start up page server.
  if (page_server_start(args->port) < 0) {
    fprintf(stderr, "[ERROR] Could not start page server\n");
    pthread_exit(NULL);
  }
  listen(page_listenfd, num_clients);
  fprintf(stderr, "[INFO] Started page server at %d\n", args->port);

  // Wait for connections from other hosts.
  int client_fd;
  int i;
  struct sockaddr_in caddr;
  socklen_t clen = sizeof(caddr);
  for (i = 0; i < num_clients; i++) {
    client_fd = accept(page_listenfd, (struct sockaddr *) &caddr, &clen);
    if (client_fd < 0) {
      fprintf(stderr, "[ERROR] page-server: error accepting client: \n\t%s\n",
              strerror(errno));
      // TODO error handling
    }
    client_msocks[i] = msg_msock_create(client_fd);
  }

  // Notify the master thread that all clients are connecte.
  dsm_page_server_all_clients_connected = true;

  // Handle page requests.

  // Setup EPOLL to wait on client socket file descriptors.
  int epfd = epoll_create(num_clients);
  if (epfd < 0) {
    fprintf(stderr, "[ERROR] epoll_create error: \n\t%s\n",
            strerror(errno));
    exit(EXIT_FAILURE);
  }
  struct epoll_event event;
  event.events = EPOLLIN; // Event triggers when read data available
  for (i = 0; i < num_clients; i++) {
    event.data.fd = client_msocks[i]->fd;
    if(epoll_ctl(epfd, EPOLL_CTL_ADD, client_msocks[i]->fd, &event)) {
      fprintf(stderr, "[ERROR] epoll_create error: \n\t%s\n",
              strerror(errno));
      exit(EXIT_FAILURE);
    }
  }

  // Loop forever handling page requests.
  msg_container_t *msg;
  while (1) {
debug_print("#### page_server debug: waiting on epoll.\n");
    // EPOLL based polling, waiting on client socket file descriptors.
    struct epoll_event events[MAX_EPOLL_EVENTS];;
    int n_events;
    int e;
    int fd;
    n_events = epoll_wait(epfd, events, MAX_EPOLL_EVENTS, -1);
    if (n_events == 0) {
      fprintf(stderr, "[ERROR] epoll_wait returned 0\n");
      exit(EXIT_FAILURE);
    }
    else if (n_events < 0) {
      if (errno == EINTR) // GDB causes this. Just try again.
        continue;
      fprintf(stderr, "[ERROR] epoll_wait error: \n\t%s\n",
              strerror(errno));
      exit(EXIT_FAILURE);
    }
    debug_print("#### page_server debug: epoll got %d events!\n", n_events);
    for(e = 0; e < n_events; e++) {
      fd = events[e].data.fd;

      // Convert fd to client_msock[?], by linear search.
      // TODO scalability fix: hash table would be better.
      struct msg_socket *cmsock = NULL;
      for (i = 0; i < num_clients; i++) {
        if (client_msocks[i]->fd == fd) {
          cmsock = client_msocks[i];
          break;
        }
      }
      assert(cmsock != NULL);

      // Handle message
      msg = msg_recv(cmsock);
      if (!msg) {
        fprintf(stderr, "[ERROR] Page server's connection to client died.\n");
        exit(EXIT_FAILURE);
      }

      switch (msg->type) {
      case MSG_APP_APP_GETPAGE:
        handle_getpage(msg, cmsock);
        break;
      case MSG_APP_APP_WAIT:
        handle_wait(msg, cmsock);
        break;
      default:
        fprintf(stderr, "[ERROR] Page server got unknown msg type %d.\n",
                        msg->type);
        exit(EXIT_FAILURE);
      }
      free(msg);
    }
  }

  pthread_exit(NULL);
}

static void handle_getpage(msg_container_t* msg, struct msg_socket *msock)
{
  // Get page
  msg_app_app_getpage* getpage_msg = (msg_app_app_getpage*) msg->body;

  // Create response
  msg_container_t* rsp = malloc(sizeof(msg_container_t) + sizeof(rsp_app_app_getpage) + getpagesize());
  rsp->type = RSP_APP_APP_GETPAGE;
  rsp->size = sizeof(msg_container_t) + sizeof(rsp_app_app_getpage) + getpagesize();
  rsp_app_app_getpage* getpage_rsp = (rsp_app_app_getpage*) rsp->body;
  getpage_rsp->page_num = getpage_msg->page_num;

  uint32_t page_num = getpage_msg->page_num; // TODO use a type.
  pthread_mutex_lock(get_local_page_lock(page_num));
  getpage_rsp->version = get_version(page_num);
  copy_local_unmodified_page_data(page_num, getpage_rsp->page_data);
  pthread_mutex_unlock(get_local_page_lock(page_num));

  getpage_rsp->client_id = get_self_client_id();
  getpage_rsp->status = DSM_SUCCESS;
  msg_send(msock, rsp);
  free(rsp);
}

static void handle_wait(msg_container_t* msg, struct msg_socket *msock)
{
  msg_app_app_wait* wait_msg = (msg_app_app_wait*) msg->body;

  // Increment the number of waiting workers.
  num_waiting_workers++;
  debug_print("Page server has %d waiters (non-self).\n", num_waiting_workers);
  // If all are waiting, send a response to all of them.
  if (num_waiting_workers == num_clients) {
    // Don't proceed until I myself am also waiting
    pthread_mutex_lock(&dsm_waiting_lock);
    while(!dsm_waiting) {
      debug_print("Waiting for myself to dsm_wait() before responding to waits.\n");
      pthread_cond_wait(&dsm_waiting_cv, &dsm_waiting_lock);
    }
    pthread_mutex_unlock(&dsm_waiting_lock);

    debug_print("Page server responding to waiters.\n");
    msg_container_t *rsp = MAKE_MSG(rsp_app_app_wait);
    rsp->type = RSP_APP_APP_WAIT;
    rsp->size = MSG_TYPE_SIZE(rsp_app_app_wait);
    int i;
    for (i = 0; i < num_clients; i++)
      msg_send (client_msocks[i], rsp); // Don't care if they fail now.
    free(rsp);

    pthread_exit(NULL);
  }
}

