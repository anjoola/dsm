/*
  Master
  ------
  Master process. Handles requests for locks and keeps track of which host
  owns what pages.
*/

#define __STDC_FORMAT_MACROS
#include <assert.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <errno.h>

#include <arpa/inet.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <sys/socket.h>
#include <pthread.h>
#include <time.h>

#include "libdsm.h"
#include "msg.h"
#include "master.h"

#define MAX_EPOLL_EVENTS 10

struct thread_args {
  msg_container_t* msg;
  struct msg_socket *msock;
};

/** Socket and port the server is listening on. */
static int listen_fd = -1;
static int port = 0;

/** Number of clients. */
static int num_clients;

/** Mapping from client ID to struct msg_socket*, IP address, and port. */
static struct msg_socket **client_msocks;
typedef struct {
  in_addr_t ip;
  uint16_t port;
} __attribute__((packed)) client_info;

client_info *clients;

/** Keep track of locks and page ownership. */
lock_table_entry *lock_table = NULL;
page_table_entry *page_table = NULL;
pthread_mutex_t lock_table_master_lock;
pthread_mutex_t page_table_master_lock;


/**
 * TODO: Robert: add comments
 */
void *handle_getlock(void* arg) {
  lock_table_entry* lock_entry;
  struct thread_args* t_args = arg;
  msg_container_t* msg = t_args->msg;
  msg_app_srv_getlock* getlock_msg = (msg_app_srv_getlock*)msg->body;
  pthread_mutex_lock(&lock_table_master_lock);
  HASH_FIND_UINT(lock_table, (uint32_t*)&getlock_msg->lock, lock_entry);
  if (lock_entry == NULL) {
    lock_entry = (lock_table_entry*)malloc(sizeof(lock_table_entry));
    lock_entry->lock_num = getlock_msg->lock;
    pthread_mutex_init(&lock_entry->lock, NULL);
    HASH_ADD_UINT(lock_table, lock_num, lock_entry);
  }
  pthread_mutex_unlock(&lock_table_master_lock);
  // Lock! Note that this will block this thread until the lock is aquired!
  pthread_mutex_lock(&lock_entry->lock);

  // Send response
  msg_container_t* rsp = MAKE_MSG(rsp_srv_app_getlock);
  rsp->type = RSP_SRV_APP_GETLOCK;
  rsp->size = sizeof(msg_container_t) + sizeof(rsp_srv_app_getlock);
  rsp_srv_app_getlock* getlock_rsp = (rsp_srv_app_getlock*)rsp->body;
  getlock_rsp->status = DSM_SUCCESS;
  msg_send(t_args->msock, rsp);
  free(rsp);
  free(msg);
  free(t_args);
  return NULL;
}

/**
 * TODO: Robert: add comments
 */
void *handle_unlock(void* arg) {
  lock_table_entry* lock_entry;
  struct thread_args* t_args = arg;
  msg_container_t* msg = t_args->msg;
  msg_app_srv_unlock* unlock_msg = (msg_app_srv_unlock*)msg->body;

  // Send response
  msg_container_t* rsp = MAKE_MSG(rsp_srv_app_unlock);
  rsp->type = RSP_SRV_APP_UNLOCK;
  rsp->size = sizeof(msg_container_t) + sizeof(rsp_srv_app_unlock);
  rsp_srv_app_unlock* unlock_rsp = (rsp_srv_app_unlock*)rsp->body;
  pthread_mutex_lock(&lock_table_master_lock);
  HASH_FIND_UINT(lock_table, (uint32_t*)&unlock_msg->lock, lock_entry);
  if (lock_entry == NULL) {
    fprintf(stderr, "ERROR: UNLOCKING before locking %u\n", unlock_msg->lock);
    unlock_rsp->status = DSM_FAIL;
  }
  else {
    pthread_mutex_unlock(&lock_entry->lock);
    unlock_rsp->status = DSM_SUCCESS;
  }
  pthread_mutex_unlock(&lock_table_master_lock);

  msg_send(t_args->msock, rsp);
  free(rsp);
  free(msg);
  free(t_args);
  return NULL;
}

/**
 * TODO: Robert: add comments
 */
void *handle_readpage(void* arg) {
  // Create response
  msg_container_t* rsp = MAKE_MSG(rsp_srv_app_readpage);
  rsp->type = RSP_SRV_APP_READPAGE;
  rsp->size = sizeof(msg_container_t) + sizeof(rsp_srv_app_readpage);
  rsp_srv_app_readpage* readpage_rsp = (rsp_srv_app_readpage*)rsp->body;

  struct thread_args* t_args = arg;
  msg_container_t* msg = t_args->msg;
  page_table_entry* page_entry;
  msg_app_srv_readpage* readpage_msg = (msg_app_srv_readpage*)msg->body;
  // Lookup page in page_table.
  pthread_mutex_lock(&page_table_master_lock);
  HASH_FIND_UINT(page_table, (uint32_t*)&readpage_msg->page_num, page_entry);
  if (page_entry == NULL || readpage_msg->version == page_entry->version) {
    // Requester has the latest version.
    readpage_rsp->status = DSM_SUCCESS;
  }
  else {
    // There is a newer version of the page available and the current
    // owner is CLIENT_ID.
    readpage_rsp->status = DSM_FAIL;
    readpage_rsp->client_id = page_entry->client_id;
  }
  pthread_mutex_unlock(&page_table_master_lock);

  msg_send(t_args->msock, rsp);
  free(rsp);
  free(msg);
  free(t_args);
  return NULL;
}

/**
 * TODO: Robert: add comments
 */
void *handle_updatepage(void* arg) {
  msg_container_t* rsp = MAKE_MSG(rsp_srv_app_updatepage);
  rsp->type = RSP_SRV_APP_UPDATEPAGE;
  rsp->size = sizeof(msg_container_t) + sizeof(rsp_srv_app_updatepage);
  rsp_srv_app_updatepage* updatepage_rsp = (rsp_srv_app_updatepage*)rsp->body;

  struct thread_args* t_args = arg;
  msg_container_t* msg = t_args->msg;
  page_table_entry* page_entry;
  msg_app_srv_updatepage* updatepage_msg = (msg_app_srv_updatepage*)msg->body;
  pthread_mutex_lock(&page_table_master_lock);
  HASH_FIND_UINT(page_table, (uint32_t*)&updatepage_msg->page_num, page_entry);

  if (page_entry == NULL) {
    page_entry = (page_table_entry*)malloc(sizeof(page_table_entry));
    page_entry->page_num = updatepage_msg->page_num;
    page_entry->client_id = updatepage_msg->client_id;
    page_entry->version = updatepage_msg->version;
    HASH_ADD_UINT(page_table, page_num, page_entry);
    updatepage_rsp->status = DSM_SUCCESS;
    updatepage_rsp->version = updatepage_msg->version;
  }
  else if (page_entry->version < updatepage_msg->version) {
    updatepage_rsp->status = DSM_SUCCESS;
    page_entry->client_id = updatepage_msg->client_id;
    page_entry->version = updatepage_msg->version;
    updatepage_rsp->version = updatepage_msg->version;
    updatepage_rsp->status = DSM_SUCCESS;
  }
  else {
    updatepage_rsp->client_id = page_entry->client_id;
    updatepage_rsp->status = DSM_FAIL;
  }

  pthread_mutex_unlock(&page_table_master_lock);
  msg_send(t_args->msock, rsp);

  free(rsp);
  free(msg);
  free(t_args);
  return NULL;
}

/**
 * TODO This message indicates that it is OK to exit now.
 *      Right now, it exits without cleaning up. Cleaner way is to count all
 *      killed connections and cleanup when all clients have sent the wait
 *      message.
 */
void *handle_wait(void* arg) {
  struct thread_args* t_args = arg;

  debug_print("Master replying to wait message.\n");
  fflush(stdout);

  msg_container_t* rsp = MAKE_MSG(rsp_srv_app_wait);
  rsp->type = RSP_SRV_APP_WAIT;
  rsp->size = MSG_TYPE_SIZE(rsp_srv_app_wait);
  msg_send(t_args->msock, rsp);

  free(t_args->msg);
  free(t_args);
  free(rsp);

  // Disconnect from all clients.
  int i;
  for(i = 0; i < num_clients; i++)
    msg_msock_close_free(&client_msocks[i]);

  debug_print("Master exiting due to WAIT (normal behavior).\n");
  fflush(stdout);
  exit(EXIT_SUCCESS);
}

//////////////////////////////////// SERVER ////////////////////////////////////


/**
 * Main loop. Listen for requests from clients for locks and pages.
 */
void main_loop(void) {
  // Setup EPOLL to wait on client socket file descriptors.
  int epfd = epoll_create(num_clients);
  if (epfd < 0) {
    fprintf(stderr, "[ERROR] epoll_create error: \n\t%s\n",
            strerror(errno));
    exit(EXIT_FAILURE);
  }
  struct epoll_event event;
  event.events = EPOLLIN; // Event triggers when read data available
  int i;
  for (i = 0; i < num_clients; i++) {
    event.data.fd = client_msocks[i]->fd;
    if(epoll_ctl(epfd, EPOLL_CTL_ADD, client_msocks[i]->fd, &event)) {
      fprintf(stderr, "[ERROR] epoll_create error: \n\t%s\n",
              strerror(errno));
      exit(EXIT_FAILURE);
    }
  }

  // Loop forever handling page requests.
  msg_container_t *msg;
  while (1) {
debug_print("#### master debug: waiting on epoll.\n");
    // EPOLL based polling, waiting on client socket file descriptors.
    struct epoll_event events[MAX_EPOLL_EVENTS];;
    int n_events;
    int e;
    int fd;
    n_events = epoll_wait(epfd, events, MAX_EPOLL_EVENTS, -1);
    if (n_events == 0) {
      fprintf(stderr, "[ERROR] epoll_wait returned 0\n");
      exit(EXIT_FAILURE);
    }
    else if (n_events < 0) {
      if (errno == EINTR) // GDB causes this. Just try again.
        continue;
      fprintf(stderr, "[ERROR] epoll_wait error: \n\t%s\n",
              strerror(errno));
      exit(EXIT_FAILURE);
    }
    debug_print("#### master debug: epoll got %d events!\n", n_events);
    for(e = 0; e < n_events; e++) {
      fd = events[e].data.fd;

      debug_print("#### master debug: event on fd %d\n", fd);
      for (i = 0; i < num_clients; i++)
         debug_print("#### master debug: client %d fd %d\n", i,
                client_msocks[i]->fd);

      // Convert fd to client_msock[?], by linear search.
      // TODO scalability fix: hash table would be better.
      int client_id = -1;
      struct msg_socket *cmsock = NULL;
      for (i = 0; i < num_clients; i++) {
        if (client_msocks[i]->fd == fd) {
          cmsock = client_msocks[i];
          client_id = i;
          break;
        }
      }
      assert(cmsock != NULL);

      // Handle the received message here
      msg = msg_recv(cmsock);
      if (!msg) {
        fprintf(stderr, "[ERROR] master's connection to client died.\n");
        exit(EXIT_FAILURE);
      }
      pthread_t handle_t = NULL;
      struct thread_args*  t_args = malloc(sizeof(struct thread_args));
      t_args->msock = cmsock;
      t_args->msg = msg;
      switch (msg->type) {
        case MSG_APP_SRV_GETLOCK:
          pthread_create(&handle_t, NULL, (void*) &handle_getlock, (void*)t_args);
          break;

        case MSG_APP_SRV_UNLOCK:
          pthread_create(&handle_t, NULL, (void*) &handle_unlock, (void*)t_args);
          break;

        case MSG_APP_SRV_READPAGE:
          pthread_create(&handle_t, NULL, (void*) &handle_readpage, (void*)t_args);
          break;

        case MSG_APP_SRV_UPDATEPAGE:
          pthread_create(&handle_t, NULL, (void*) &handle_updatepage, (void*)t_args);
          break;

        case MSG_APP_SRV_WAIT:
          //pthread_create(&handle_t, NULL, (void*) &handle_wait, (void*)t_args);
          handle_wait(t_args);
          break;

        // TODO some message to know when to stop?
        default:
          fprintf(stderr, "[ERROR] master got unknown message type %d\n", msg->type);
          break;
      }
      if (handle_t)
        pthread_detach(handle_t);
    }
  }
}

/**
 * Waits for clients to connect and send their connection information.
 * Responds to each client by sending a list of the IP addresses and ports
 * for all clients.
 *
 * returns: 0 on success, -1 otherwise.
 */
int client_connect() {
  struct msg_socket **client_msocks_unordered =
      calloc(num_clients, sizeof(struct msg_sock*));
  client_msocks = calloc(num_clients, sizeof(struct msg_sock*));
  clients = calloc(num_clients, sizeof(client_info));
  listen(listen_fd, num_clients);

  // Wait for all clients to connect first.
  int fd, num = 0;
  struct sockaddr_in caddr;
  socklen_t clen = sizeof(caddr);
  while (num < num_clients) {
    fd = accept(listen_fd, (struct sockaddr *) &caddr, &clen);
    if (fd < 0)
      return -1;

    // This is a temporary unordered mapping of client msg_sockets, arranged
    // by the order they attempted to connect.
    // We eventually want to make it into a client_id -> msg_socket mapping.
    client_msocks_unordered[num] = msg_msock_create(fd);
    num++;
  }

  // Wait for message from clients to get their IP address, port, and ID.
  // Create the client_id -> msg_socket mapping.
  msg_container_t *msg;
  for (num = 0; num < num_clients; num++) {
    msg = msg_recv(client_msocks_unordered[num]);

    if (msg->type == MSG_APP_SRV_INIT) {
      msg_app_srv_init *info = (msg_app_srv_init *) msg->body;
      int client_id = info->client_id;

      // Store client details.
      client_msocks[client_id] = client_msocks_unordered[num];
      clients[client_id].ip = info->ip;
      clients[client_id].port = info->port;
    }
    else {
      fprintf(stderr, "Master: Got unexpected first packet from client, "
                      "type: %d\n", msg->type);
    }
    free(msg);
  }

  // Construct list of client information.
  size_t msg_size = sizeof(msg_container_t) + sizeof(rsp_srv_app_details) +
                    num_clients * sizeof(client_info); // TODO size?
  msg_container_t *rsp = calloc(msg_size, 1);
  rsp->type = RSP_SRV_APP_DETAILS;
  rsp->size = msg_size;
  rsp->hash = 0;

  rsp_srv_app_details *details = (rsp_srv_app_details *) rsp->body;
  details->num_clients = num_clients;
  memcpy(details->array, clients, num_clients * sizeof(client_info));

  // Send them to all clients.
  for (num = 0; num < num_clients; num++) {
    msg_send(client_msocks[num], rsp);
  }
  return 0;
}

/**
 * Open socket and wait for clients to connect.
 *
 * port: Port to start server on.
 * returns: 0 on success, -1 otherwise.
 */
int server_start(int port) {
  pthread_mutex_init(&lock_table_master_lock, NULL);
  pthread_mutex_init(&page_table_master_lock, NULL);

  // Open socket to accept connections.
  listen_fd = socket(AF_INET, SOCK_STREAM, 0);
  if (listen_fd < 0) {
    fprintf(stderr, "[ERROR] Could not open socket.\n");
    return -1;
  }

  // Allow reuse of port without waiting. Otherwise get bind errors
  // on back-to-back runs.
  int one = 1;
  setsockopt(listen_fd, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(int));

  // Bind specified port to socket.
  struct sockaddr_in saddr;
  saddr.sin_family = AF_INET;
  saddr.sin_addr.s_addr = INADDR_ANY;
  saddr.sin_port = htons(port);
  if (bind(listen_fd, (struct sockaddr *) &saddr, sizeof(saddr)) < 0) {
    fprintf(stderr, "[ERROR] Could not bind.\n");
    return -1;
  }

  fprintf(stderr, "[INFO] Started master at %d\n", port);

  // Wait for all clients to connect.
  if (client_connect() < 0)
    return -1;

  return 0;
}

/**
 * Forks off the master server.
 *
 * _port: Port to start master server.
 * _num_clients: Number of clients.
 */
void master_exec(int _port, int _num_clients) {
  if (fork() == 0) {
    port = _port;
    num_clients = _num_clients;

    server_start(port);
    main_loop();

    // TODO(agong): Master exit? Communicate back to deployer?
  }
}
