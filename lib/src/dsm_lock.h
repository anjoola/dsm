#include <unistd.h>
#include "utlist.h"

int dsm_lock(uint32_t lock);
int dsm_unlock(uint32_t lock);
