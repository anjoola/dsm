#include "uthash.h"

/** Entry in the lock table. */
typedef struct lock_table_entry_t {
  uint32_t lock_num;        // Used as a key
  pthread_mutex_t lock;
  UT_hash_handle hh;        // Makes this structure hashable
} lock_table_entry;

/** Entry in the page table. */
typedef struct page_table_entry_t {
  uint32_t page_num;        // Used as a key
  uint32_t client_id;       // ID of host who owns this page
  uint32_t version;         // Version number, to determine most recent copy
  UT_hash_handle hh;        // Makes this structure hashable
} page_table_entry;


void *handle_getlock(void* arg);
void *handle_unlock(void* arg);
void *handle_readpage(void* arg);
void *handle_updatepage(void* arg);

/**
 * Forks off the master server.
 *
 * _port: Port to start master server.
 * _num_clients: Number of clients.
 */
void master_exec(int _port, int _num_clients);
