#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include "dsm_page_data.h"
#include "libdsm.h"
#include "msg.h"

msg_container_t* _get_page(uint32_t page_num, uint32_t client_id);
int _update_page(uint32_t page_num, uint32_t client_id);
int _read_page(uint32_t page_num, uint32_t client_id);

/**
 * Attempts to "update a page", meaning creating a new version.
 * Synchronizes with the latest version first if need be.
 *
 * 1) Attempts to create a new version, by incremening its current version
 *    and sending to master.
 * 2a) If master accepts, update state to new version, cleanup state, and
 *    return. Otherwise:
 * 2b) If master rejects because version conflict, master responds
 *    with the latest owner. Get page from latest owner, apply our
 *    changes on top of that latest version. Restart at step 1.
 *
 * TODO FIXME: This routine has a potential starvation type problem.
 * TODO FIXME: Ideally master would provide a version for this commit,
 *             and this worker would do any needed patching (with proper
 *             blocking until completion).
 */
// TODO FIXME: This should be called "push page", or "commit page", etc
void dsm_update_page(uint32_t page_num){
  debug_print("Started at page version: %d\n", get_version(page_num));
  while (true) {
    msg_container_t* msg = MAKE_MSG(msg_app_srv_updatepage);
    msg->type = MSG_APP_SRV_UPDATEPAGE;
    msg->size = sizeof(msg_container_t) + sizeof(msg_app_srv_updatepage);

    // Send master "updatepage" with current version + 1, this is hopefully
    // the new version.
    msg_app_srv_updatepage* updatepage_msg = (msg_app_srv_updatepage*) msg->body;
    updatepage_msg->client_id = get_host_id();
    updatepage_msg->page_num = page_num;

    pthread_mutex_lock(get_local_page_lock(page_num));

    updatepage_msg->version = get_version(page_num) + 1;
    msg_container_t* rsp = msg_send_recv(get_master_msock(), msg);
    free(msg);
    if (!rsp) {
      fprintf(stderr, "ERROR: UPDATE_PAGE request go no reponse");
      exit(EXIT_FAILURE);
    }

    // Check the response. FAIL means there is a version conflict and we need
    // to pull the changes from the master and try again. Otherwise it was
    // successful and we can finish up the commit.
    rsp_srv_app_updatepage* updatepage_rsp = (rsp_srv_app_updatepage*)rsp->body;
    if (updatepage_rsp->status == DSM_FAIL) {
      // Pull changes, then try again.
      _update_page(page_num, updatepage_rsp->client_id);

      pthread_mutex_unlock(get_local_page_lock(page_num));
      free(rsp);
    }
    else {
      // We have the latest version. Finish commit.
      assert(updatepage_rsp->version == get_version(page_num) + 1);
      update_shadow_copy(page_num, ptindex2addr(page_num));
      set_version(page_num, updatepage_rsp->version);

      pthread_mutex_unlock(get_local_page_lock(page_num));
      free(rsp);
      break;
    }
  }
  debug_print("Updated page to version: %d\n", get_version(page_num));
  // TODO FIXME: Should clear the modified flag, remove pt entry, and
  // TODO FIXME: re-enable protection. ACTUALLY this function should be
  // TODO FIXME: called from a dsm_release_all() function that does these things.
}

int _update_page(uint32_t page_num, uint32_t client_id) {
  msg_container_t* rsp = _get_page(page_num, client_id);
  if (!rsp) {
    return -1;
  }
  rsp_app_app_getpage* getpage_rsp = (rsp_app_app_getpage*)rsp->body;
  if (getpage_rsp->status == DSM_FAIL) {
    free(rsp);
    assert(getpage_rsp->status != DSM_FAIL);
    return -1;
  }
  else {
    patch_page(page_num, getpage_rsp->version, getpage_rsp->page_data);
    free(rsp);
    return 0;
  }
}

/**
 * Get a page from client CLIENT_ID, without concern for version.
 * Return the response message (with data and version) for whatever use needed.
 * Response needs to be freed by caller.
 * TODO: describe what the response can be.
 */
msg_container_t* _get_page(uint32_t page_num, uint32_t client_id) {
  /* Fetch page from remote client */
  msg_container_t* msg = MAKE_MSG(msg_app_app_getpage);
  msg->type = MSG_APP_APP_GETPAGE;
  msg->size = sizeof(msg_container_t) + sizeof(msg_app_app_getpage);

  msg_app_app_getpage* getpage_msg = (msg_app_app_getpage*) msg->body;
  getpage_msg->page_num = page_num;
  msg_container_t* rsp = msg_send_recv(get_sock(client_id), msg);
  free(msg);
  return rsp;
}

int patch_page(uint32_t page_num, uint32_t remote_version, uint8_t* remote_page) {
  uint8_t* shadow_copy = get_shadow_copy(page_num);
  uint8_t* local_page = ptindex2addr(page_num);
  size_t i = 0;
  size_t pgsize = getpagesize();

  for (; i < pgsize; i++) {
    if (local_page[i] == shadow_copy[i])
      local_page[i] = remote_page[i];
  }
  // Update shadow copy
  update_shadow_copy(page_num, remote_page);
  return set_version(page_num, remote_version);
}

/**
 * Checks if a newer version of the page is available, and if so, gets it
 * and inserts it underneath our modifications. (I.e. it patches our local
 * changes on top of the latest version.)
 * (Note: if no version exists, no overwriting occurs.)
 */
void dsm_pull_page(uint32_t page_num){
  // First ask the master if my version is the latest.
  msg_container_t* msg = MAKE_MSG(msg_app_srv_readpage);
  msg->type = MSG_APP_SRV_READPAGE;
  msg->size = sizeof(msg_container_t) + sizeof(msg_app_srv_readpage);
  msg_app_srv_readpage* readpage_msg = (msg_app_srv_readpage*) msg->body;
  readpage_msg->client_id = get_host_id();
  readpage_msg->page_num = page_num;

  pthread_mutex_lock(get_local_page_lock(page_num));

  readpage_msg->version = get_version(page_num);
  msg_container_t* rsp = msg_send_recv(get_master_msock(), msg);
  free(msg);
  if (!rsp) {
    fprintf(stderr, "ERROR: READ_PAGE request got no reponse");
    exit(EXIT_FAILURE);
  }

  // If my version is the latest (DSM_SUCCESS), do nothing. Otherwise,
  // read the page from the owner (client_id).
  rsp_srv_app_readpage* readpage_rsp = (rsp_srv_app_readpage*)rsp->body;
  if (readpage_rsp->status == DSM_FAIL) {

    //TODO need to lock the page during this operation?

    if(get_modified(page_num))
      _update_page(page_num, readpage_rsp->client_id);
    else
      _read_page(page_num, readpage_rsp->client_id);

    // TODO handle failure. If failure is because ownership changed, should
    // probably repeat this function?
  }

  pthread_mutex_unlock(get_local_page_lock(page_num));

  free(rsp);
}

/**
 * Fetch a page from client CLIENT_ID, and overwrite the local page and update
 * version. Returns 0 on failure, -1 on success.
 *
 * TODO: Why does this fail? Ownership moved? Caller needs to know.
 */
int _read_page(uint32_t page_num, uint32_t client_id) {
  msg_container_t* rsp = _get_page(page_num, client_id);
  if (!rsp) {
    return -1;
  }
  rsp_app_app_getpage* getpage_rsp = (rsp_app_app_getpage*)rsp->body;
  if (getpage_rsp->status == DSM_FAIL) {
    free(rsp);
    return -1;
  }
  else {
    uint8_t* local_page = ptindex2addr(page_num);
    memcpy(local_page, getpage_rsp->page_data, getpagesize());
    if (get_shadow_copy(page_num))
      update_shadow_copy(page_num, getpage_rsp->page_data);
    set_version(page_num, getpage_rsp->version);
    free(rsp);
    return 0;
  }
}
