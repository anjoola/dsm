#include <stdint.h>
#include "uthash.h"

/** Arguments needed in order to start the page server. */
typedef struct ps_args_t {
  uint16_t port;              // Port to start on
  int num_hosts;              // Number of hosts (including this one)
} ps_args;

/** Global variable notifies other threads that the clients are connected. */
extern bool dsm_page_server_all_clients_connected;

/**
 * Main thread function for the page server. Serves requests from other
 * applications for memory.
 */
void *page_server(void *_args);
