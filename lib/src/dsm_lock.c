/*
 * Handles locking by sending messages to and from the master.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "dsm_lock.h"
#include "dsm_page_data.h"
#include "libdsm.h"
#include "msg.h"
#include "uthash.h"

/**
 * Acquire a mutual exclusion lock. All code between this call and
 * dsm_unlock(lock) with the same lock id number will be treated as a
 * critical section across all machines. Forces an acquire of all pages.
 *
 * lock: Lock ID.
 * returns: 0 on success, exits on failure.
 */
int dsm_lock(uint32_t lock) {
  // Create message.
  msg_container_t* msg = MAKE_MSG(msg_app_srv_getlock);
  msg->type = MSG_APP_SRV_GETLOCK;
  msg->size = sizeof(msg_container_t) + sizeof(msg_app_srv_getlock);

  // Set fields for lock message.
  msg_app_srv_getlock* getlock_msg = (msg_app_srv_getlock*)msg->body;
  getlock_msg->lock = lock;
  getlock_msg->client_id = dsm_id;

  // Send message to master.
  msg_container_t* rsp = msg_send_recv(get_master_msock(), msg);
  free(msg);
  if (!rsp) {
    fprintf(stderr, "[ERROR] Socket to master closed while getting lock.\n");
    exit(EXIT_FAILURE);
  }

  // Get reply from master.
  rsp_srv_app_getlock* getlock_rsp = (rsp_srv_app_getlock*)rsp->body;
  if (getlock_rsp->status == DSM_FAIL) {
    fprintf(stderr, "[ERROR] FAILED to get lock %d\n", lock);
    free(rsp);
    exit(EXIT_FAILURE);
  }
  free(rsp);

  // Acquire all pages.
  dsm_acquire_all();

  return 0;
}

/**
 * Release a lock. See dsm_lock(). Forces a release of all pages.
 *
 * lock: Lock ID.
 * returns: 0 on success, exits on failure.
 */
int dsm_unlock(uint32_t lock) {
  // Release all pages.
  dsm_release_all();

  // Create message.
  msg_container_t* msg = MAKE_MSG(msg_app_srv_unlock);
  msg->type = MSG_APP_SRV_UNLOCK;
  msg->size = sizeof(msg_container_t) + sizeof(msg_app_srv_unlock);

  // Set fields for unlock message.
  msg_app_srv_unlock* unlock_msg = (msg_app_srv_unlock*)msg->body;
  unlock_msg->lock = lock;
  unlock_msg->client_id = dsm_id;

  // Send message to master.
  msg_container_t* rsp = msg_send_recv(get_master_msock(), msg);
  free(msg);
  if (!rsp) {
    fprintf(stderr, "[ERROR] Socket to master closed while releasing lock.\n");
    exit(EXIT_FAILURE);
  }

  // Get reply from master.
  rsp_srv_app_unlock* unlock_rsp = (rsp_srv_app_unlock*)rsp->body;
  if (unlock_rsp->status == DSM_FAIL) {
    fprintf(stderr, "[ERROR] Failed unlock %d\n", lock);
    free(rsp);
    exit(EXIT_FAILURE);
  }

  free(rsp);
  return 0;
}
