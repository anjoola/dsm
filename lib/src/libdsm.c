#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <signal.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <pthread.h>
#include <unistd.h>

#include <msg.h>
#include "libdsm.h"
#include "page_server.h"

#define MAX_PORT 65536

// Number of extra arguments added to the binary: server_name, server_port,
// host_id, num_hosts.
#define NUM_EXTRA_ARGS 4

#define MAX_NUM_TRIES 10

/** SIGSEGV handlers. */
static void dsm_install_default_sigsegv (void);
static void dsm_sigsegv_action_wrapper (int sig, siginfo_t *info, void *ucp);
// TODO move to header file, expose to user.
int dsm_sigsegv_action (siginfo_t *info);

// Thread ids
pthread_t thread_main;
pthread_t thread_page_server;

// Whether or not dsm_start() has been called.
bool dsm_started = false;
// Whether dsm_wait() has been called on this host or not;
bool dsm_waiting = false;
pthread_mutex_t dsm_waiting_lock;
pthread_cond_t dsm_waiting_cv;

/** Master connection. */
char *dsm_server_name = "localhost";
uint16_t dsm_server_port = 0;

/** My own details. */
int dsm_id = 0; // 0 for master, > 0 otherwise.
int dsm_n_hosts = 0;
int dsm_port = 0;

/** Connection file descriptors. */
struct msg_socket *master_msock = NULL;
struct msg_socket **worker_msocks = NULL; // Array, indexed by id number,
                                          // of msocks to each page server.

/** The shared region. */
static void *dsm_addr = NULL;
static size_t dsm_length = 0;

// Pagetable entry
typedef enum {DSM_AL_NONE, DSM_AL_READ, DSM_AL_READWRITE} dsm_access_level_t;
typedef struct ptent {
  // Lock protects everything about a page, including this pt entry, and the
  // data. Prevents races between the application thread and the page server.
  // This lock may be overly fine grained. Using a smaller fixed sized set
  // of locks based on hash of page number may be sufficient.
  pthread_mutex_t lock;

  dsm_access_level_t acclev;
  bool inmem;
  bool modified;
  bool side_access; // Also in shadow buffer, so page_server can touch while
                    // protected.
  uint32_t version;
  uint8_t* shadow_copy; // != NULL implies modified == true
} ptent_t;

ptent_t *dsm_pagetable = NULL;
size_t dsm_pagetable_size = 0; // Size in entries

UT_icd ptindex_t_icd  = {sizeof(ptindex_t), NULL, NULL, NULL };


/////////////////////////////////// HELPERS ////////////////////////////////////

/**
 * Debug print. Only prints if DEBUG_PRINT_YES is defined.
 */
void debug_print(char *fmt, ...) {
  #ifdef DEBUG_PRINT_YES
    va_list args;
    va_start(args, fmt);
    printf(fmt, args);
    va_end(args);
  #endif
}

/**
 * Gets the socket file descriptor for the given host ID.
 *
 * host_id: Host ID to check.
 * returns: A msg_socket containing the socket details, or NULL if this host
 *          is not connected or we cannot find this host.
 */
struct msg_socket *get_sock(uint32_t host_id) {
  // Cannot find specified host.
  if (host_id >= dsm_n_hosts)
    return NULL;

  return worker_msocks[host_id];
}

/**
 *Gets the msg_socket of the master server.
 */
struct msg_socket *get_master_msock() {
  return master_msock;
}

/**
 * Returns this host's own ID.
 */
uint32_t get_host_id() {
  return dsm_id;
}

/**
 * Gets the IP address associated with a given hostname.
 *
 * hostname: The hostname to connect to (e.g. "corn23", "localhost").
 *
 * returns: An ip_addr containing the IP address, or 0 if it cannot be found.
 *          Result is in network-order.
 */
in_addr_t ip_from_hostname(const char *hostname) {
  // Entry for the provided host.
  char ip[INET_ADDRSTRLEN];
  struct hostent *entry;
  struct in_addr **addr_list;
  int i;

  // Could not find a host with the given name.
  entry = gethostbyname(hostname);
  if (entry == NULL) {
    fprintf(stderr, "[ERROR] Could not resolve %s!\n", hostname);
    return 0;
  }

  // Get the first IP address from the found entry.
  else {
    addr_list = (struct in_addr **) entry->h_addr_list;
    for (i = 0; addr_list[i] != NULL; i++) {
      strcpy(ip, inet_ntoa(*addr_list[i]));
    }
    return inet_addr(ip);
  }
}

/**
 * Gets the client's own IP address.
 *
 * returns: An in_addr containing the IP address, or 0 if it cannot be found.
 *          Result is in network-order.
 */
in_addr_t ip_self(void) {
  struct ifaddrs *addrs = NULL, *iface;
  in_addr_t ip_addr = 0;
  getifaddrs(&addrs);

  // Need to find the IP address from the correct interface.
  char *correct_iface = "eth";
  char check_iface[100];

  // Go through each interface.
  for (iface = addrs; iface != NULL; iface = iface->ifa_next) {
    if (iface->ifa_addr && iface->ifa_addr->sa_family == AF_INET) {
      // Check if this is from the correct interface.
      memcpy(check_iface, iface->ifa_name, strlen(iface->ifa_name) - 1);
      if (strcmp(check_iface, correct_iface) != 0)
        continue;

      ip_addr = ((struct sockaddr_in *) iface->ifa_addr)->sin_addr.s_addr;
    }
  }
  freeifaddrs(addrs);
  return ip_addr;
}

/**
 * Helper to get the page table index for a specified page.
 *
 * addr: Address of a page.
 * returns: A page table index.
 */
ptindex_t addr2ptindex (void *addr)
{
  if (dsm_addr && addr >= dsm_addr && addr < dsm_addr + dsm_length)
    return (addr - dsm_addr) / getpagesize();
  else
    return -1;
}

/**
 * Helper to get the first address of the nth page of the shared region.
 */
void *ptindex2addr (ptindex_t n)
{
  if (n >= 0 && n < dsm_length / getpagesize())
    return n * getpagesize() + dsm_addr;
  else
    return NULL;
}

/* mmap wrapper with error handling. */
void *Mmap(void *addr, size_t length, int prot, int flags,
           int fd, off_t offset)
{
  if(addr != mmap(addr, length, prot, flags, fd, offset)) {
    fprintf(stderr, "mmap error: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }
}

/* mprotect wrapper with error handling. */
int Mprotect(void *addr, size_t len, int prot)
{
  if(mprotect(addr, len, prot)) {
    fprintf(stderr, "mprotect error: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }
}


////////////////////////////////// DSM LIBRARY /////////////////////////////////

/**
 * Connect to the master server and send own connection details. Receive
 * connection details of other hosts and save them. Then connect to them.
 */
static void master_and_page_servers_connect() {
  // Connect to master server. The master server may not be up yet so we keep
  // trying a few times.
  int num_tries = 0;
  while (master_msock == NULL && (num_tries++) < MAX_NUM_TRIES) {
    master_msock = msg_connect(dsm_server_name, dsm_server_port, false);

    if (master_msock == NULL)
      sleep(1);
  }

  // Send message to master about own connection info (IP address, port, ID).
  msg_container_t *msg = MAKE_MSG(msg_app_srv_init);
  msg->type = MSG_APP_SRV_INIT;
  msg->size = sizeof(msg_container_t) + sizeof(msg_app_srv_init);
  msg->hash = 0;
  msg_app_srv_init *info = (msg_app_srv_init *) msg->body;

  info->client_id = dsm_id;
  info->ip = ip_self();
  info->port = dsm_port;
  msg_container_t *rsp = msg_send_recv(master_msock, msg);
  if (!msg) {
    fprintf(stderr, "[ERROR] Connection to master dead.\n");
    exit(EXIT_FAILURE);
  }
  free(msg);

  // Get details of other hosts from master.
  assert(rsp->type == RSP_SRV_APP_DETAILS);
  rsp_srv_app_details *details = (rsp_srv_app_details *) rsp->body;
  assert(details->num_clients == dsm_n_hosts);

  // Connect to all hosts.
  size_t worker_msocks_size = sizeof(struct msg_msock*) * dsm_n_hosts;
  worker_msocks = calloc(worker_msocks_size, 1);

  int i, host_fd;
  in_addr_t *ip;
  uint16_t *port;
  void *host;
  for (i = 0; i < dsm_n_hosts; i++) {
    // Don't connect to self.
    if (i == dsm_id) continue;

    host = (char *) details->array + i * (sizeof(in_addr_t) + sizeof(uint16_t));
    ip = (in_addr_t *) host;
    port = (uint16_t *) (host + sizeof(in_addr_t));

    // Connect to host.
    assert(worker_msocks[i] == NULL);
    num_tries = 0;
    while (worker_msocks[i] == NULL && (num_tries++) < MAX_NUM_TRIES) {
      worker_msocks[i] = msg_connect_ip(*ip, *port, false);
      if (worker_msocks[i] == NULL) {
        sleep(1);
        // TODO error handling if cannot connect to someone?
      }
    }
  }
  fprintf(stderr, "[INFO] Connected to all page servers!\n");

  free(rsp);
}

/**
 * Start/initialize dsm. Assume binary is deployed on all other hosts. Now
 * synchronize hosts and start up page server thread. Also assumes that
 * the shared memory has already been specified by dsm_share().
 *
 * The arguments are the args as passed to the program's main(argc, argv).
 * This assumes the last 4 arguments are (starting from the leftmost):
 *    server_name - The hostname or IP of worker with id 0.
 *    server_port - The port that worker with id 0 will listen on.
 *    worker_id   - A unique id number, 0 to (num_workers - 1) to identify
 *                  this host
 *    num_workers - Is the number of workers that will run.
 *
 * ERRORS
 *   ENOMEDIUM  Shared region not set up first (via call to dsm_share).
 *   ENOTSUP    Application should be deployed via the deployment server.
 */
int dsm_start(int argc, char *argv[]) {
  // Check if shared region set up.
  if (!dsm_pagetable) {
    fprintf(stderr, "[ERROR] No shared regions setup before dsm_start.\n");
    errno = ENOMEDIUM;
    return -1;
  }

  // Was this invoked correctly?
  if (argc < NUM_EXTRA_ARGS + 1) {
    fprintf(stderr, "[ERROR] App should be invoked via dsm_server.\n");
    errno = ENOTSUP;
    return -1;
  }

  // Parse arguments to get location of master and own ID.
  dsm_server_name = argv[argc - 4];
  dsm_server_port = atoi(argv[argc - 3]);
  dsm_id = atoi(argv[argc - 2]);
  dsm_n_hosts = atoi(argv[argc - 1]);

  // Initialize port (for page server).
  dsm_port = (dsm_server_port + 1 + dsm_id) % MAX_PORT;

  thread_main = pthread_self();

  // Master application. Start server to handle page and lock requests.
  // Do before we open any file descriptors since fork will duplicate them.
  if (dsm_id == 0)
    master_exec(dsm_server_port, dsm_n_hosts);

  // Setup stuff shared with page_server
  pthread_mutex_init(&dsm_waiting_lock, NULL);
  pthread_cond_init(&dsm_waiting_cv, NULL);

  // Set up the page_server thread, if there are multiple hosts.
  if (dsm_n_hosts > 1) {
    ps_args *args = calloc(sizeof(ps_args) + dsm_n_hosts * sizeof(int), 1);
    args->port = dsm_port;
    args->num_hosts = dsm_n_hosts;

    int r = pthread_create(&thread_page_server, NULL, page_server, (void *)args);
    if (r) {
      fprintf(stderr, "Error spawning page server thread: %d\n", r);
      exit(EXIT_FAILURE);
    }
    // TODO somehow handle if page server dies early?

    // Connect to the master server and page servers. Master server used to ask
    // for locks and page owners. Page servers used to get pages from owners.
    master_and_page_servers_connect();

    // Wait for the other clients to connect to the page server
    debug_print("[DSM] Waiting for all clients to connect to page server.\n");
    while (!dsm_page_server_all_clients_connected) {
      debug_print("Waiting...\n");
      struct timespec t1, t2;
      t1.tv_sec = 0;
      t1.tv_nsec = 1000*1000;
      nanosleep(&t1 , &t2); // If fails, don't care.
    }
    debug_print("[DSM] All clients connected to page server.\n");
  }
  else {
    assert(dsm_id == 0);
  }

  dsm_started = true;
  return 0;
}

/**
 * Meant to copy the conventions of mmap().
 *
 * ERRORS
 *   EINVAL  We don't like addr or length (e.g., they are too
 *           large, or not aligned on a page boundary).
 */
int dsm_share(void *addr, long length)
{
  size_t pgsize = getpagesize();
  if ((size_t)addr % pgsize || (size_t)length % pgsize) {
    errno = EINVAL;
    return -1;
  }

  // Check to see if already sharing another region. (Can't do multiple.)
  if (dsm_pagetable) {
    fprintf(stderr, "[ERROR] dsm library can only share 1 region.\n");
    exit(EXIT_FAILURE);
  }

  dsm_addr = addr;
  dsm_length = length;

  dsm_pagetable_size = length / pgsize;
  dsm_pagetable = calloc(sizeof(*dsm_pagetable), dsm_pagetable_size);

  // Init mutexes as recursive type.
  pthread_mutexattr_t atts;
  pthread_mutexattr_init(&atts);
  pthread_mutexattr_settype(&atts, PTHREAD_MUTEX_RECURSIVE);
  ptent_t *ptent = &dsm_pagetable[0];
  long i;
  for (i = 0; i < dsm_pagetable_size; i++) {
    pthread_mutex_init(&ptent->lock, &atts);
    ptent++;
  }

  // Initialize modified_pages
  utarray_new(modified_pages, &ptindex_t_icd);

  // TODO expose an option for this?
  dsm_install_default_sigsegv();

  return 0;
}


/////////////////////////////////// HANDLERS ///////////////////////////////////

/*
 * Install the default SIGSEGV handler, overwriting any previous handler.
 * This is a convenience for the user if they do not want to install their
 * own handler. Otherwise they must call handle_sigsegv themselves.
 */
static void dsm_install_default_sigsegv (void)
{
  // Handle only SIGSEGV.
  sigset_t ss;
  if (sigemptyset(&ss)
      || sigaddset(&ss, SIGSEGV)) {
    fprintf(stderr, "sigset error: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }
  struct sigaction act;
  act.sa_sigaction = dsm_sigsegv_action_wrapper;
  act.sa_mask = ss;
  act.sa_flags = SA_SIGINFO; // use 3-arg handler
  struct sigaction oldact; // unused
  if (sigaction(SIGSEGV, &act, &oldact)) {
    fprintf(stderr, "sigaction error: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }
}

/* SIGSEGV handler. Interfaces directly with sigaction. */
static void dsm_sigsegv_action_wrapper (int sig, siginfo_t *info, void *ucp)
{
  assert(1 == dsm_sigsegv_action(info));
}

/* SIGSEGV handler. Cannot be used directly as a sigaction handler, which
 * requires a handler with a different signature. (Use a wrapper as needed.)
 *
 * Returns 1 if the sigsegv region was within the
 * dsm shared region and handled by this handler. Otherwise return 0.
 * This can be used as part of a larger, custom handling scheme.
 */
int dsm_sigsegv_action (siginfo_t *info)
{
  debug_print("Start SIGSEGV handler.\n");
  assert(info->si_signo == SIGSEGV);

  // Check thread. Presently assume application does not spawn multiple
  // threads, only that this library will spawn the page-server thread,
  // which should never SIGSEGV.
  pthread_t self = pthread_self();
  if (self != thread_main) {
    if (self == thread_page_server)
      fprintf(stderr, "Error: page server thread SIGSEGVed.\n");
    else
      fprintf(stderr, "Error: SIGSEGV in unknown thread. Application must not"
                      " spawn additional threads!\n");
    exit(EXIT_FAILURE);
  }

  void *addr = info->si_addr;
  debug_print("fault addr: %p\n", addr);

  ptindex_t ind = addr2ptindex(addr);
  debug_print("fault index: %d\n", (int)ind);

  // If not in a dsm page, return 0, some other handler might deal with it.
  if (ind < 0)
    return 0;

  if (!dsm_started) {
    fprintf(stderr, "[ERROR] Accessed shared memory before dsm_start().\n");
    exit(EXIT_FAILURE);
  }

  addr = (void *)(((size_t)addr / getpagesize()) * getpagesize());
  debug_print("page addr: %p\n", addr);

  ptent_t *ptent = &dsm_pagetable[ind];
  switch (ptent->acclev) {
  case DSM_AL_NONE:
    debug_print("Hit page with access level NONE, upgrading to READ.\n");
    // Map memory if needed, update data to latest version (with any
    // local uncommitted changes patched on top), and upgrade to
    // read-only access.
    pthread_mutex_lock(&ptent->lock);
    if (!ptent->inmem)
      Mmap(addr, getpagesize(), PROT_READ | PROT_WRITE,
           MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, 1, 0);
    else {
      // If it's in memory, and there is no access, it must be in the
      // shadow copy, to let the page server get it (otherwise it can't
      // be protected).
      assert(ptent->side_access);
      assert(ptent->shadow_copy);
    }
    // Update data to latest version (with any local uncommitted changes
    // patched on top). Only do this if running multiple hosts.
    // Potential optimization: Only do this if there is new data.
    Mprotect(addr, getpagesize(), PROT_READ | PROT_WRITE);
    if (dsm_n_hosts > 1)
      dsm_pull_page(ind);

    // TODO: If the data is already modified, maybe we should just go
    // TODO: straight to write-mode?
    Mprotect(addr, getpagesize(), PROT_READ);

    ptent->inmem = true;
    ptent->acclev = DSM_AL_READ;
    ptent->side_access = false;
    pthread_mutex_unlock(&ptent->lock);
    break;

  case DSM_AL_READ:
    debug_print("Hit page with access level READ, upgrading to READWRITE.\n");
    pthread_mutex_lock(&ptent->lock);
    assert(ptent->inmem);
    // Upgrade to read-only access.
    // TODO dsm management operations
    Mprotect(addr, getpagesize(), PROT_READ | PROT_WRITE);
    ptent->acclev = DSM_AL_READWRITE;
    // Ensure shadow copy, set as modified.
    // It could already be modified  if user did acquire -> modify -> acquire
    if (!ptent->modified) {
      // Create shadow copy if needed.
      if (!ptent->shadow_copy) {
        ptent->shadow_copy = malloc(getpagesize());
        memcpy(ptent->shadow_copy, addr, getpagesize());
      }
      ptent->modified = true;
      utarray_push_back(modified_pages, &ind);
    }
    else {
      assert(ptent->shadow_copy);
    }
    pthread_mutex_unlock(&ptent->lock);
    break;

  default:
    fprintf(stderr, "ERROR: SIGSEGV on read/write memory?"
                    " Trying to execute it?\n");
    exit(EXIT_FAILURE);
  }

  debug_print("End SIGSEGV handler.\n");
  return 1;
}

/**
 * Acquire all pages.
 *
 * This performs an acquire in our "acquire/release" semantics. Effectively
 * this is done lazily. This operation protects all pages, forcing the first
 * access to read the latest version, and merge changes.
 */
void dsm_acquire_all(void)
{
  ptent_t *ptent = &dsm_pagetable[0];
  void *addr = ptindex2addr(0);
  size_t ent_num;
  // TODO: For scalability, keep a list of accessable pages.
  for (ent_num = 0; ent_num < dsm_pagetable_size; ent_num++) {
    pthread_mutex_lock(&ptent->lock);
    if (ptent->acclev != DSM_AL_NONE) {
      assert(ptent->inmem);
      if (!ptent->modified) {
        // Create a shadow page for side_access (so that the page_server
        // can read the data while page is protected).
        ptent->shadow_copy = malloc(getpagesize());
        memcpy(ptent->shadow_copy, addr, getpagesize());
      }
      else {
        assert(ptent->shadow_copy);
      }
      Mprotect(addr, getpagesize(), 0);
      ptent->acclev = DSM_AL_NONE;
      ptent->side_access = true;
    }
    pthread_mutex_unlock(&ptent->lock);
    ptent++;
    addr += getpagesize();
  }
}

/**
 * Release all pages.
 *
 * Performs a release in our "acquire/release" semantics. Rebases on latest
 * copy from the master, and pushes the newest commit. Keeps page data locally,
 * but reverts to unmodified state (with protection against writes). Reads
 * remain unprotected until acquire.
 */
void dsm_release_all(void)
{
  ptindex_t* p = NULL;
  uint32_t page_num;

debug_print("Release start\n");
  while (p = (ptindex_t*)utarray_back(modified_pages)){
    page_num = *p;
    ptent_t *ptent = &dsm_pagetable[page_num];
    // Remove modified state.
    debug_print("release, checking page %d\n", page_num);
    assert(ptent->modified);
    assert(ptent->shadow_copy);

    // Merge and push latest version.
    Mprotect(ptindex2addr(page_num), getpagesize(), PROT_READ | PROT_WRITE);
    ptent->acclev = DSM_AL_READWRITE;
    dsm_update_page(page_num);

    pthread_mutex_lock(&ptent->lock);
    // TODO, debug use only! Slow!
    assert(!memcmp(ptent->shadow_copy, ptindex2addr(page_num), getpagesize()));
    free(ptent->shadow_copy);
    ptent->shadow_copy = NULL;
    ptent->modified = false;
     // Protect ensures we detect modifications after clearing modified_pages
    Mprotect(ptindex2addr(page_num), getpagesize(), PROT_READ);
    ptent->acclev = DSM_AL_READ;
    pthread_mutex_unlock(&ptent->lock);

    utarray_pop_back(modified_pages);
  }
debug_print("Release end\n");
}

/**
 * Wait for all other workers to finish, then kill the DSM.
 */
void dsm_wait(void)
{
  // Check for resources
  assert(dsm_pagetable);

  debug_print("dsm_wait: messaging self\n");

  // Tell my own server that I am waiting
  pthread_mutex_lock(&dsm_waiting_lock);
  dsm_waiting = true;
  pthread_cond_broadcast(&dsm_waiting_cv);
  pthread_mutex_unlock(&dsm_waiting_lock);

  // Create the WAIT message
  msg_container_t* msg = MAKE_MSG(msg_app_app_wait);
  msg->type = MSG_APP_APP_WAIT;
  msg->size = sizeof(msg_container_t) + sizeof(msg_app_app_wait);
  msg_app_app_wait* wait_msg = (msg_app_app_wait*) msg->body;
  wait_msg->client_id = get_host_id();

  debug_print("dsm_wait: messaging all workers\n");

  // Send the WAIT message to all servers.
  assert(worker_msocks != NULL);
  int i;
  for (i = 0; i < dsm_n_hosts; i++) {
    // Don't have a connection to self.
    if (i == dsm_id) {
      debug_print("dsm_wait: skipping self %d\n", i);
      continue;
    }

    debug_print("dsm_wait: messaging %d\n", i);
    if (!worker_msocks[i]
        || 0 > msg_send(worker_msocks[i], msg))
    {
      fprintf(stderr, "ERROR: Couldn't send msg WAIT to worker %d.\n", i);
      exit(EXIT_FAILURE);
    }
  }
  free(msg);
  msg = NULL;

  //
  // From this point on we don't report any errors if any connections are
  // lost. We expect that to happen!
  //

  debug_print("dsm_wait: Waiting for responses\n");

  // Wait for all page servers to respond, and then close those connections.
  for (i = 0; i < dsm_n_hosts; i++) {
    // Don't have a connection to self.
    if (i == dsm_id) {
      debug_print("dsm_wait: Skipping self response %d\n", i);
      continue;
    }

    debug_print("dsm_wait: Waiting for response from %d\n", i);
    msg_container_t *rsp = msg_recv(worker_msocks[i]);

    debug_print("dsm_wait: %s get a response from %d\n", rsp ? "did" : "DIDNT", i);
    // We may not get a response if others die first? Anyways, it's not
    // important at this point.
    if (rsp) {
      if (rsp->type != RSP_APP_APP_WAIT) {
        fprintf(stderr, "ERROR: Page server %d returned type %d on wait "
                        "request.\n", i, rsp->type);
        exit(EXIT_FAILURE);
      }
      free(rsp);
    }
    debug_print("dsm_wait: closing connection to worker %d\n", i);
    msg_msock_close_free(&worker_msocks[i]);
  }

  debug_print("dsm_wait: telling master to die\n");

  // Tell the master to die, wait for some kind of response to make sure
  // the connection stays open until master gets it, then close the connection.
  msg = MAKE_MSG(msg_app_srv_wait);
  msg->type = MSG_APP_SRV_WAIT;
  msg->size = MSG_TYPE_SIZE(msg_app_srv_wait);
  assert(master_msock);
  msg_container_t *rsp = msg_send_recv(master_msock, msg);
  if (rsp)
    free(rsp);
  msg_msock_close_free(&master_msock);
  free(msg);

  //
  // Free/Cleanup resources.
  //
  debug_print("dsm_wait: freeing worker_msocks\n");
  free(worker_msocks);
  worker_msocks = NULL;

  debug_print("dsm_wait: checking locks\n");

  // Check to see if all locks are unlocked.
  size_t j;
  for (j = 0; j < dsm_pagetable_size; j++) {
    // If we can try the lock, that means either nobody owns it, or this
    // thread owns it (because it is recursive).
    if(0 == pthread_mutex_trylock(&dsm_pagetable[j].lock)) {
      pthread_mutex_unlock(&dsm_pagetable[j].lock);
    }
    else {
      fprintf(stderr, "[ERROR] Page server still holds a page lock, or there\n"
                      "        is some other error: %s!!\n", strerror(errno));
    }
    pthread_mutex_destroy(&dsm_pagetable[j].lock);
  }

debug_print("dsm_wait: freeing shadow pages\n");

  // Free shadow pages.
  for (j = 0; j < dsm_pagetable_size; j++) {
    if(dsm_pagetable[j].shadow_copy)
      free(dsm_pagetable[j].shadow_copy);
  }

debug_print("dsm_wait: freeing page table\n");

  // Free page table.
  free(dsm_pagetable);
  dsm_pagetable = NULL;

  debug_print("dsm_wait completed successfully!\n");
}

/**
 * Gets the local unmodified page data for the specified page number,
 * i.e. the most recent version I have (without unversioned changes).
 *
 * page_num: The page number.
 */
void copy_local_unmodified_page_data(uint32_t page_num, void * out_buffer)
{
  void *addr = ptindex2addr(page_num);

  assert(dsm_pagetable);
  assert(page_num < dsm_pagetable_size);

  ptent_t *ptent = &dsm_pagetable[page_num];

  // Must be in memory.
  assert(ptent->inmem);
  // If we are trying to get an inaccessible page, there must be side-access
  // via shadow copy.
  assert(ptent->acclev != DSM_AL_NONE
         || (ptent->side_access && ptent->shadow_copy));
  // If it is modified, there must be a shadow copy.
  assert(!ptent->modified || ptent->shadow_copy);

  if(ptent->modified || ptent->acclev == DSM_AL_NONE)
    addr = ptent->shadow_copy;
  else
    addr = ptindex2addr(page_num);

  memcpy(out_buffer, addr, getpagesize());
}

/////////////////////////////// PAGES AND LOCKS ////////////////////////////////

pthread_mutex_t *get_local_page_lock(ptindex_t page_num) {
  return &dsm_pagetable[page_num].lock;
}

uint8_t* get_shadow_copy(ptindex_t page_num) {
  return dsm_pagetable[page_num].shadow_copy;
}

void update_shadow_copy(ptindex_t page_num, uint8_t* page_data) {
  memcpy(dsm_pagetable[page_num].shadow_copy, page_data, getpagesize());
}

uint32_t get_version(uint32_t page_num) {
  return dsm_pagetable[page_num].version;
}

int set_version(uint32_t page_num, uint32_t version) {
  return dsm_pagetable[page_num].version = version;
  return 0;
}

bool get_modified(uint32_t page_num) {
  return dsm_pagetable[page_num].modified;
}

uint32_t get_self_client_id() {
  return dsm_id;
}
