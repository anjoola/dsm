#ifndef _LIBDSM_H_
#define _LIBDSM_H_

#include <ifaddrs.h>
#include <netdb.h>
#include <stdint.h>
#include <stdbool.h>

#include "msg.h"
#include "utarray.h"

typedef ssize_t ptindex_t;

/** My own details. */
extern int dsm_id; // 0 for master, > 0 otherwise.

// Whether dsm_wait() has been called on this host or not.
// Shared between main app thread, and page-server.
extern bool dsm_waiting;
extern pthread_mutex_t dsm_waiting_lock;
extern pthread_cond_t dsm_waiting_cv;

/**
 * Debug print. Only prints if DEBUG_PRINT_YES is defined.
 */
void debug_print(char *fmt, ...);

/**
 * Gets the socket file descriptor for the given host ID.
 *
 * host_id: Host ID to check.
 * returns: Message socket to master, or NULL on failure.
 */
struct msg_socket *get_sock(uint32_t host_id);


struct msg_socket *get_master_msock();


/**
 * Returns this host's own ID.
 */
uint32_t get_host_id();

/**
 * Gets the IP address associated with a given hostname.
 *
 * hostname: The hostname to connect to (e.g. "corn23", "localhost").
 *
 * returns: An ip_addr containing the IP address, or 0 if it cannot be found.
 *          Result is in network-order.
 */
in_addr_t ip_from_hostname(const char *hostname);

/**
 * Gets the local unmodified page data for the specified page number,
 * i.e. the most recent version I have (without unversioned changes).
 *
 * page_num: The page number.
 */
void copy_local_unmodified_page_data(uint32_t page_num, void * out_buffer);



pthread_mutex_t *get_local_page_lock(ptindex_t page_num);
uint8_t* get_page(uint32_t page_num);
int write_page(uint32_t page_num, uint8_t* page_data);
uint32_t get_version(uint32_t page_num);
int set_version(uint32_t page_num, uint32_t version);
bool get_modified(uint32_t page_num);
uint8_t* get_shadow_copy(ptindex_t page_num);
void update_shadow_copy(ptindex_t page_num, uint8_t* page_data);
ptindex_t addr2ptindex(void *addr);
void *ptindex2addr(ptindex_t n);
uint32_t get_self_client_id();
UT_array *modified_pages;

#endif // _LIBDSM_H_
