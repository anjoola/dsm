/* Header file for applications to include when using the library. */

#include <stdint.h>
#include <stdlib.h>

/**
 * Start/initialize dsm. Assume binary is deployed on all other hosts. Now
 * synchronize hosts and start up page server thread. Also assumes that
 * the shared memory has already been specified by dsm_share().
 *
 * The arguments are the args as passed to the program's main(argc, argv).
 * This assumes the last 4 arguments are (starting from the leftmost):
 *    server_name - The hostname or IP of worker with id 0.
 *    server_port - The port that worker with id 0 will listen on.
 *    worker_id   - A unique id number, 0 to (num_workers - 1) to identify
 *                  this host
 *    num_workers - Is the number of workers that will run.
 *
 * ERRORS
 *   ENOMEDIUM  Shared region not set up first (via call to dsm_share).
 *   ENOTSUP    Application should be deployed via the deployment server.
 */
int dsm_start(int argc, char *argv[]);

/**
 * Meant to copy the conventions of mmap().
 *
 * ERRORS
 *   EINVAL  We don't like addr or length (e.g., they are too
 *           large, or not aligned on a page boundary).
 */
int dsm_share(void *addr, size_t length);

/**
 * Acquire all pages.
 *
 * This performs an acquire in our "acquire/release" semantics. This will
 * force an update of any shared page read afterwards.
 */
void dsm_acquire_all(void);

/**
 * Release all pages.
 *
 * Performs a release in our "acquire/release" semantics. Rebases on latest
 * copy from the master, and pushes the newest commit. Keeps page data locally,
 * but reverts to unmodified state (with protection against writes). Reads
 * remain unprotected until acquire.
 */
void dsm_release_all(void);

/**
 * Acquire a mutual exclusion lock. All code between this call and
 * dsm_unlock(lock) with the same lock id number will be treated as a
 * critical section across all machines. Forces an acquire of all pages.
 *
 * lock: Lock ID.
 * returns: 0 on success, exits on failure.
 */
int dsm_lock(uint32_t lock);

/**
 * Release a lock. See dsm_lock(). Forces a release of all pages.
 *
 * lock: Lock ID.
 * returns: 0 on success, exits on failure.
 */
int dsm_unlock(uint32_t lock);
