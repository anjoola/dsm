#ifndef _BINARY_UTILS_H_
#define _BINARY_UTILS_H_

#include <assert.h>
#include <errno.h>
#include <getopt.h>
#include <ifaddrs.h>
#include <netdb.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <sys/socket.h>
#include <sys/stat.h>

#include "msg.h"


// 2 MB limit for binaries.
#define MAX_BINARY_SIZE 2097152

// Maximum size of the binary name and command-line arguments, in characters.
#define MAX_BINARY_CHARS 2000

// Maximum number of characters in a port or ID string.
#define MAX_NUM_CHARS 5

/**
 * A binary consists of its name, arguments, and the actual bytes. Since all
 * are variably-sized, we need to split it into 3 structs.
 */
struct binary_name {
  uint16_t id;         /* Binary ID. 0 for the master, > 0 for clients. */
  uint16_t port;       /* Port of the master app. */
  uint16_t num_hosts;  /* Number of total hosts. */
  uint16_t len;        /* Length of binary name (including null terminator). */
  char name[];         /* Name of binary. */
};
struct binary_args {
  uint16_t num_args;   /* Number of "arguments". Not the actual number of
                          arguments to the binary but the number of array
                          elements that would come in argv. */
  uint16_t len;        /* Length of string (including null terminator). */
  char args[];         /* String containing the arguments. */
};
struct binary {
  uint32_t len;        /* Number of bytes in binary. */
  char binary[];       /* Bytes of the binary. */
};

/**
 * Creates a binary and incorporates it into a struct so that it can be
 * sent over the network.
 *
 * name: The name of the binary.
 * args: String of arguments.
 * num_args: The number of arguments.
 * size: A pointer to a variable to store the size of the resulting struct.
 * port: Port of the master app.
 * num_hosts: Number of total hosts running the binary.
 *
 * returns: A pointer to the struct containing the binary details. This must
 *          be freed.
 */
char *create_binary(char *name, char *args, int num_args, uint32_t *size,
                    uint16_t port, uint16_t num_hosts) {
  // Keep running total of length needed for struct.
  uint32_t total_len = 0;

  // Needed for the binary name.
  uint32_t name_struct_len = sizeof(struct binary_name) +
                             sizeof(char) * (strlen(name) + 1);
  total_len += name_struct_len;

  // Binary's arguments.
  uint32_t args_struct_len = sizeof(struct binary_args);
  if (args != NULL)
    args_struct_len += strlen(args) + 1;
  total_len += args_struct_len;

  // Read in the actual binary.
  int status;
  struct stat stat_buf;
  FILE *fp = fopen(name, "r");
  status = stat(name, &stat_buf);
  if (fp == NULL || status != 0 || S_ISDIR(stat_buf.st_mode)) {
    fprintf(stderr, "[ERROR] Binary does not exist!\n");
    return NULL;
  }
  int bytes = 0;
  char buf[MAX_BINARY_SIZE];
  char *buf_ptr = buf;
  uint32_t bin_len = 0;
  while ((bytes = fread(buf_ptr, 1, MAX_BINARY_SIZE, fp)) > 0) {
    bin_len += bytes;
    buf_ptr += bytes;
  }
  fclose(fp);

  uint32_t binary_struct_len = sizeof(struct binary) + bin_len;
  total_len += binary_struct_len;
  *size = total_len;

  // Create binary struct and set the fields.
  char *bin = calloc(total_len, 1);

  // Binary name (including master's port).
  struct binary_name *bin_name = (void *) bin;
  bin_name->port = port;
  bin_name->num_hosts = num_hosts;
  bin_name->len = strlen(name) + 1;
  strncpy(bin_name->name, name, strlen(name) + 1);

  // Binary arguments.
  struct binary_args *bin_args = (void *) bin + name_struct_len;
  bin_args->num_args = num_args;
  if (args != NULL) {
    bin_args->len = strlen(args) + 1;
    strncpy(bin_args->args, args, strlen(args) + 1);
  }

  // Binary contents.
  struct binary *actual_bin = (void *) bin + name_struct_len + args_struct_len;
  actual_bin->len = bin_len;
  memcpy(actual_bin->binary, buf, bin_len);

  return bin;
}

/**
 * Gets the name of the binary to execute. The resulting char array must
 * be freed.
 *
 * packet: The packet from the server.
 * len: Length of the packet.
 * returns: The binary name, or NULL if it could not be parsed.
 */
char *get_name(char *packet, uint32_t len) {
  struct binary_name *bin_name = (struct binary_name *) packet;
  uint16_t name_len = bin_name->len;

  // Packet received is not long enough to contain the full name.
  if (len < name_len) {
    return NULL;
  }

  char *name = calloc(name_len, sizeof(char));
  strncpy(name, bin_name->name, name_len);
}

/**
 * Gets the number of "arguments" in a string. An argument is separated by
 * a space. e.g. "-z 3 -d 5" is actually 4 arguments.
 *
 * args: The string containing the arguments.
 * returns: The number of arguments in the string.
 */
int get_num_args(char *args) {
  int num = 0;
  if (args == NULL)
    return num;

  int arg_len = strlen(args);
  char args_list[arg_len];
  strncpy(args_list, args, arg_len);
  char *arg = strtok(args_list, " ");

  // Count the number of arguments.
  while (arg != NULL) {
    num++;
    arg = strtok(NULL, " ");
  }

  return num;
}

/**
 * Gets the arguments of the binary as an array of strings. Adds on the server
 * and port so the application can continue communicating with the server. The
 * resulting array must be freed.
 *
 * packet: The packet from the server.
 * len: Length of the packet.
 * server_name: Name of the server.
 *
 * returns: A pointer to the beginning of the string array.
 */
char **get_args(char *packet, uint32_t len, char *server_name) {
  struct binary_name *bin_name = (struct binary_name *) packet;
  uint16_t name_struct_len = sizeof(struct binary_name) +
                             sizeof(char) * bin_name->len;
  struct binary_args *args = (struct binary_args *)(packet + name_struct_len);
  uint16_t args_len = args->len;

  // Packet received is not long enough to contain the arguments.
  if (len < args_len) {
    return NULL;
  }

  // Allocate array to contain pointers to strings. Add 6: the program name,
  // the server name, server port, host ID, number of hosts, and 1 for null.
  char **array = calloc(args->num_args + 6, sizeof(char *));
  int i = 0;

  // Program name is first argument.
  array[i++] = get_name(packet, len);

  // Get the actual arguments to the program.
  char *args_list = args->args;
  char *args_tok = strtok(args_list, " ");
  while (i < args->num_args + 1 && args_tok != NULL) {
    int arg_len = strlen(args_tok);
    char *new_arg = calloc(arg_len, sizeof(char));
    strncpy(new_arg, args_tok, arg_len);
    array[i] = new_arg;

    i++;
    args_tok = strtok(NULL, " ");
  }

  // Extra arguments needed for DSM.
  char *port = calloc(MAX_NUM_CHARS, 1);
  char *id = calloc(MAX_NUM_CHARS, 1);
  char *num_hosts = calloc(MAX_NUM_CHARS, 1);
  sprintf(port, "%d", bin_name->port);
  sprintf(id, "%d", bin_name->id);
  sprintf(num_hosts, "%d", bin_name->num_hosts);

  array[i++] = server_name;
  array[i++] = port;
  array[i++] = id;
  array[i++] = num_hosts;
  array[i++] = NULL;
  return array;
}

/**
 * Frees the arguments.
 */
void free_args(char **args) {
  char **args_head = args;
  while (args != NULL) {
    free(*args);
    args++;
  }
  free(args_head);
}

/**
 * Writes the binary out to a file so it can be executed.
 *
 * pocket: The packet from the server.
 * len: Length of the packet.
 * returns: Whether or not the binary was written out successfully.
 */
bool write_binary(char *packet, uint32_t len) {
  struct binary_name *bin_name = (struct binary_name *) packet;
  uint16_t name_len = sizeof(struct binary_name) + sizeof(char) * bin_name->len;
  struct binary_args *args = (struct binary_args *)(packet + name_len);
  uint16_t args_len = sizeof(struct binary_args) + sizeof(char) * args->len;
  struct binary *bin = (struct binary *)(packet + args_len + name_len);

  // Packet received is not long enough to contain the arguments.
  if (len < bin->len)
    return false;

  // Get the name for the binary and write it out.
  char *name = calloc(bin_name->len, 1);
  strncpy(name, bin_name->name, bin_name->len);
  FILE *fp = fopen(name, "w");
  size_t bytes = fwrite(bin->binary, 1, bin->len, fp);
  fclose(fp);

  // Give execute permissions to owner.
  if (chmod(name, S_IRUSR | S_IWUSR | S_IXUSR))
    fprintf(stderr, "[ERROR] Could not chmod binary to add execute"
                    " permissions.\n");
  free(name);
  return bytes == bin->len;
}

/**
 * Forks and execs the given binary.
 *
 * msg: Message containing the binary.
 * server_name: Name of the main server.
 */
void execute_binary(msg_container_t *msg, char *server_name) {
  // Get contents of binary and write out to file.
  char *bin = msg->body;
  int bytes = MSG_BODY_SIZE(*msg);
  char *name = get_name(bin, bytes);
  char **args = get_args(bin, bytes, server_name);
  if (!write_binary(bin, bytes)) {
    fprintf(stderr, "[ERROR] Could not create binary to execute.\n");
    return;
  }

  // Prepend "./" to the name.
  char *dotted_name = malloc(2 + strlen(name) + 1);
  sprintf(dotted_name, "./%s", name);
  free(name);

  // Execute binary.
  if (fork() == 0) {
    execvp(dotted_name, args);

    free_args(args);
    free(dotted_name);
    fprintf(stderr, "[ERROR] failed execvp(): %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }
  // Parent.
  else {
    free(dotted_name);
  }
}

#endif // _BINARY_UTILS_H_
