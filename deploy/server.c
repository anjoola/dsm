#include "binary_utils.h"

/*
 Deployer. Main source of interaction with DSM library. Starts apps by deploying
 binary to daemons.
*/

#define MAX_PORT 65536
#define PROMPT "deployer> "

/** Valid commands. */
#define DEPLOY_CMD "deploy"
#define EXIT_CMD "exit"

/** Socket and port the server is listening on. */
static int listen_fd;
static int port = 0;

/** Number of daemons. */
static int num_daemons = 0;

/** Message sockets for the daemons. An array of struct msg_socket *. */
static struct msg_socket **daemon_msocks = NULL;

/**
 * Waits for daemons to connect. Assumes a socket has already been opened on
 * the server.
 *
 * returns: 0 on success, -1 otherwise.
 */
int daemon_connect() {
  if (num_daemons == 0) {
    fprintf(stderr, "No daemons to wait for\n");
    return;
  }

  listen(listen_fd, num_daemons);
  fprintf(stderr, "Waiting for daemons...\n");

  // Wait for all daemons to connect first.
  int clientfd;
  struct sockaddr_in caddr;
  socklen_t clen = sizeof(caddr);
  int num_connected = 0;
  while (num_connected < num_daemons) {
    assert(daemon_msocks[num_connected] == NULL);
    clientfd = accept(listen_fd, (struct sockaddr *)&caddr, &clen);
    if (clientfd < 0) {
      fprintf(stderr, "[ERROR] Daemon could not connect.\n");
      return -1;
    }
    daemon_msocks[num_connected] = msg_msock_create(clientfd);
    fprintf(stderr, "Daemon %d connected.\n", (num_connected + 1));
    num_connected++;
  }
  fprintf(stderr, "All daemons connected.\n");
  return 0;
}

/**
 * Open socket and wait for daemons to connect.
 *
 * returns: 0 on success, -1 otherwise.
 */
int server_start(void) {
  // Open socket to accept connections.
  listen_fd = socket(AF_INET, SOCK_STREAM, 0);
  if (listen_fd < 0) {
    fprintf(stderr, "[ERROR] Could not open socket.\n");
    return -1;
  }

  // Allow reuse of port without waiting. Otherwise get bind errors
  // on back-to-back runs.
  int one = 1;
  setsockopt(listen_fd, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(int));

  // Bind specified port to socket.
  struct sockaddr_in saddr;
  saddr.sin_family = AF_INET;
  saddr.sin_addr.s_addr = INADDR_ANY;
  saddr.sin_port = htons(port);
  if (bind(listen_fd, (struct sockaddr *) &saddr, sizeof(saddr)) < 0) {
    fprintf(stderr, "[ERROR] Could not bind.\n");
    return -1;
  }

  // Wait for all daemons to connect.
  if (daemon_connect() < 0)
    return -1;

  return 0;
}

/**
 * Sends binaries to all daemons and starts the master binary.
 *
 * bin_name: Name of the binary.
 * bin_args: Arguments for the binary.
 *
 * returns: 0 on success, -1 otherwise.
 */
int send_and_exec_binary(char *bin_name, char *bin_args) {
  uint16_t port = rand() % MAX_PORT;

  // Create the binary to send to all the daemons.
  uint32_t bin_size;
  char *bin = create_binary(bin_name, bin_args, get_num_args(bin_args),
                            &bin_size, port, num_daemons + 1);
  if (bin == NULL)
    return -1;

  // Send binaries to all daemons.
  size_t msg_size = sizeof(msg_container_t) + bin_size;
  msg_container_t *msg = calloc(msg_size, 1);
  msg->type = MSG_SRV_DMN_DEPLOY;
  msg->size = msg_size;
  msg->hash = 0; // Unused.

  // Start master binary on the same machine.
  ((struct binary_name *) bin)->id = 0;
  memcpy(msg->body, bin, bin_size);
  execute_binary(msg, "localhost");

  // Send a binary with a different ID to each client.
  int i;
  char id[5];
  for (i = 0; i < num_daemons; i++) {
    ((struct binary_name *) bin)->id = i + 1;
    memcpy(msg->body, bin, bin_size);
    if (msg_send(daemon_msocks[i], msg)) {
      fprintf(stderr, "Could not send binary to daemon %d\n", (i + 1));
      return -1;
    }
  }

  free(msg);
  return 0;
}

/**
 * Server exit function. Handle cleanup.
 */
void server_exit() {
  // Message to send to daemons to die.
  size_t msg_size = sizeof(msg_container_t);
  msg_container_t *msg = calloc(msg_size, 1);
  msg->type = MSG_SRV_DMN_KILL;
  msg->size = msg_size;
  msg->hash = 0; // Unused.

  // Kill daemons and close sockets.
  int i;
  for (i = 0; i < num_daemons; i++) {
    if (msg_send(daemon_msocks[i], msg))
      fprintf(stderr, "Could not kill daemon %d\n", (i + 1));

    msg_msock_close_free(&daemon_msocks[i]);
  }
  free(daemon_msocks);
  close(listen_fd);
  free(msg);
  exit(0);
}

/**
 * Main server loop.
 */
void server_loop() {
  char buf[MAX_BINARY_CHARS];
  char *token;
  int r;

  while (1) {
    memset(buf, 0, MAX_BINARY_CHARS);
    printf(PROMPT);
    fflush(stdout);

    // Get command to run. Remove newline.
    r = read(STDIN_FILENO, buf, MAX_BINARY_CHARS);
    if (r <= 0 || strlen(buf) == 1) continue;
    *(buf + strlen(buf) - 1) = '\0';
    token = strtok(buf, " ");

    // Deploy binary.
    if (strcmp(token, DEPLOY_CMD) == 0) {
      char *bin_name = strtok(NULL, " ");
      if (bin_name == NULL) {
        fprintf(stderr, "[ERROR] Invalid command\n");
        continue;
      }

      char *bin_args = strtok(NULL, " ");
      if (bin_args != NULL)
        *(bin_args + strlen(bin_args)) = ' ';
      send_and_exec_binary(bin_name, bin_args);
    }

    // Kill deployer.
    else if (strcmp(token, EXIT_CMD) == 0) {
      fprintf(stderr, "[INFO] Exiting\n");
      server_exit();
    }

    else {
      fprintf(stderr, "[ERROR] Invalid command\n");
    }
  }
}

/**
 * Prints a usage message.
 */
static void usage() {
  fprintf(stderr,
    "\nUsage: server -n num_daemons -p port\n"
    "       num_daemons = 0 only if running app on server\n"
  );
  exit(1);
}

int main(int argc, char *argv[]) {
  // Parse arguments.
  struct option options[] = {
    { "daemons", required_argument, NULL, 'n' },
    { "port", required_argument, NULL, 'p' },
    { NULL, 0, NULL, 0 }
  };

  int opt;
  while ((opt = getopt_long(argc, argv, "n:p:", options, NULL)) != -1) {
    switch (opt) {
    case 'n':
      num_daemons = atoi(optarg);
      break;
    case 'p':
      port = atoi(optarg);
      break;
    default:
      usage();
      break;
    }
  }

  // See if all arguments filled in.
  if (num_daemons < 0 || port == 0)
    usage();
  daemon_msocks = calloc(num_daemons, sizeof(struct msg_socket *));

  // Start server and send binaries to daemons.
  srand(time(NULL));
  if (server_start() < 0) {
    return 1;
  }

  // Do server loop.
  server_loop();
  return 0;
}
