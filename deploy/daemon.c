#include <poll.h>

#include "binary_utils.h"

/** Socket used to communicate with server. */
static struct msg_socket *msock;

/** Server details. */
char *server_name;
char *port_str;

/**
 * Exit function. Do cleanup for daemon.
 */
void daemon_exit() {
  // Close socket.
  msg_msock_close_free(&msock);

  fprintf(stderr, "[INFO] Exiting\n");
  exit(EXIT_SUCCESS);
}

/**
 * Main daemon loop. Wait for binaries from the server and execute them.
 */
void daemon_loop() {
  // Wait to receive message from server.
  msg_container_t *msg;

  // Polling struct to wait on data on sockfd.
  struct pollfd pfd;
  pfd.fd = msock->fd;
  pfd.events = POLLIN;

  while (1) {
    // Poll for message on sockfd, wait forever.
    int npolled = poll(&pfd, 1, -1);
    if (npolled != 1) {
      fprintf(stderr, "[ERROR] poll returned %d, error: \n\t%s\n",
              npolled, strerror(errno));
      exit(EXIT_FAILURE);
    }

    msg = msg_recv(msock);
    if (msg == NULL) {
      fprintf(stderr, "Master disconnected, closing connection\n");
      exit(EXIT_FAILURE);
    }

    switch (msg->type) {
    // Start a new binary.
    case MSG_SRV_DMN_DEPLOY:
      execute_binary(msg, server_name);
      break;

    // Done.
    case MSG_SRV_DMN_KILL:
      free(msg);
      daemon_exit(); // Doesn't return
      break;
    default:
      break;
    }

    free(msg);
  }
}

/**
 * Prints a usage message.
 */
static void usage() {
  fprintf(stderr,
    "\nUsage: daemon -s server_name -p server_port\n"
  );
  exit(1);
}

int main(int argc, char *argv[]) {
  int port = 0;

  // Parse arguments.
  struct option options[] = {
    { "server", required_argument, NULL, 's' },
    { "port", required_argument, NULL, 'p' },
    { NULL, 0, NULL, 0 }
  };

  int opt;
  while ((opt = getopt_long(argc, argv, "s:p:", options, NULL)) != -1) {
    switch(opt) {
    case 's':
      server_name = optarg;
      break;
    case 'p':
      port_str = optarg;
      port = atoi(optarg);
      break;
    default:
      usage();
      break;
    }
  }

  // Validate arguments.
  if (server_name == NULL)
    server_name = "localhost";
  if (port == 0)
    usage();

  // Connect to server.
  msock = msg_connect(server_name, port, true);
  if (msock == NULL) {
    return 1;
  }

  daemon_loop();
  return 0;
}
