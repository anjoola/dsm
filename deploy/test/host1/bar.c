#include <dsm.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <sys/types.h>
#include <time.h>

void *shared_mem_addr;
int *array;

void print_timediff(struct timespec time_start, struct timespec time_finish)
{
  uint64_t nanodiff
       = time_finish.tv_nsec + time_finish.tv_sec * (1000*1000*1000)
       - (time_start.tv_nsec + time_start.tv_sec * (1000*1000*1000));
  int secs = nanodiff / (1000*1000*1000);
  long nsecs = nanodiff % (1000*1000*1000);

  double f_msecs = nanodiff / (1000.0 * 1000.0);

  printf("Time difference: %f msecs\n", f_msecs);
}

int rand_sleep(int max_milliseconds) {
  int ms = rand()%max_milliseconds;
  struct timespec t1, t2;
  t1.tv_sec = 0;
  t1.tv_nsec = ms * 1000*1000;
  nanosleep(&t1 , &t2); // If fails, don't care
  return ms;
}

int main(int argc, char *argv[])
{

  printf("starting bar\n");
  int i;
  for (i = 0; i < argc; i++) {
    printf("got arg: %s\n", argv[i]);
  }
  for (i=2; i >= 0; i--) {
    printf("Waiting %d more seconds...\n", i);
    sleep(1);
  }

  shared_mem_addr = (void*)(((size_t)9000000/getpagesize())*getpagesize());
  dsm_share(shared_mem_addr, getpagesize() * 4);
  dsm_start(argc, argv);

  srand(time(NULL) + getpid()); // Try and seed workers independently

  array = shared_mem_addr;

  for(i = 0; i < 10; i++) {
    struct timespec time_start, time_finish;
    clock_gettime(CLOCK_MONOTONIC, &time_start);

    printf("LOCK %d\n", dsm_lock(1));

    clock_gettime(CLOCK_MONOTONIC, &time_finish);
    print_timediff(time_start, time_finish);

    array[0] += 10;
    printf("COUNTER: %d $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n", array[0]);

    // Sleep for a little bit, to help test the lock mutual exclusion.
    int slept_ms = rand_sleep(100);
    printf("Inner sleep time: %d msecs\n", slept_ms);

    printf("UNLOCK %d\n", dsm_unlock(1));

    // Sleep for a little bit, to help test the lock mutual exclusion.
    slept_ms = rand_sleep(100);
    printf("Outer sleep time: %d msecs\n", slept_ms);

  }

  // Pause a random amount of time up to 9 seconds. This is to get each worker
  // to finish at a different time, to test dsm_wait.
  int pre_close_secs = rand() % 10;
  for (i=pre_close_secs; i >= 0; i--) {
    printf("Waiting %d more seconds...\n", i);
    sleep(1);
  }

  dsm_wait();

  exit(EXIT_SUCCESS);
}
