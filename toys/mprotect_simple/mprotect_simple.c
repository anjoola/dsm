/*
  mprotect_simple

  A simple demonstration of mprotect to interpose on memory access.

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !!!! Address Space Randomization Note !!!
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  per https://gcc.gnu.org/wiki/Randomization

  Call this on the command line first to prevent address space randomization
  in programs invoked from this bash session:

  > setarch $(uname -m) -RL bash

*/
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sys/mman.h>
#include <malloc.h>
#include <signal.h>

/*
  Shared region setup

  1 page, located at approximately halfway between stack and heap
  observed addresses. These may need to change on other systems.
  Also may not work with address randomization! See note at top of file.
  Note the first console outputs of the program are pointer addresses
  from the stack to help figure this out.
*/
void *base = (void *)0x400000000000;
size_t len = 4*1024;

/* SIGSEGV handler for interposing on memory. */
void handle_sigsegv (int sig)
{
  printf("Start SIGSEGV handler.\n");
  // Must unprotect the memory before we touch it, or bad things happen.
  mprotect(base, len, PROT_WRITE);
  // Insert an integer  value at the first address of the protected region.
  int *i = base;
  *i = 1234567;
  printf("End SIGSEGV handler.\n");
}

int main()
{
  // Diagnostic message to help see where is safe to mmap.
  int s;
  int *h = malloc(sizeof(h));
  printf("stack @ %p, heap @ %p\n", &s, h);

  // Set up signal handler for SIGSEGV
  sigset_t ss;
  if (sigemptyset(&ss)
      || sigaddset(&ss, SIGSEGV)) {
    fprintf(stderr, "sigset error: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }
  struct sigaction act, oldact;
  act.sa_handler = handle_sigsegv;
  act.sa_mask = ss;
  act.sa_flags = 0;
  if (sigaction(SIGSEGV, &act, &oldact)) {
    fprintf(stderr, "sigaction error: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }

  // Setup some memory
  int *p = mmap(base, len, PROT_READ | PROT_WRITE,
                MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, 1, 0);
  printf("mmap @ %p.\n", p);
  if (p == (void *)-1) {
    fprintf(stderr, "mmap error: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }

  // Print the value of the integer at the beginning of the mmap region.
  // Should initially be zero
  printf("Original value: %d\n", *p);

  // Protecting the memory should force the signal handler to be called
  // on any access.
  if (mprotect(base, len, PROT_NONE)) {
    fprintf(stderr, "mprotect error: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }

  // Printing this same integer again should print whatever is assigned
  // in the signal handler.
  int i = *p;
  printf("Value after mprotect: %d\n", i);

  return(0);
}
